package com.sinmobile.gogorgeos.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Commons;

import butterknife.Bind;


public class ContactUsActivity extends CommonActivity implements View.OnClickListener {

    //RelativeLayout imv_back;
    EditText  edt_name, edt_email, edt_phone, edt_comment;
    TextView edt_phoneNumber, edt_supportEmail,txv_send;
    ImageView imv_facebook, imv_twitter, imv_youtube, imv_instagram, imv_snap;
    ImageView ui_imvBack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        edt_phoneNumber = (TextView)findViewById(R.id.edt_phone_number);
        edt_phoneNumber.setText(Commons.PHONENUMBER);

        edt_supportEmail = (TextView)findViewById(R.id.edt_support);
        edt_supportEmail.setText(Commons.EMAIL);

        edt_name = (EditText)findViewById(R.id.edt_name);
        edt_email = (EditText)findViewById(R.id.edt_email);
        edt_phone = (EditText)findViewById(R.id.edt_phone);
        edt_comment = (EditText)findViewById(R.id.edt_comments);

        imv_facebook = (ImageView)findViewById(R.id.imv_facebook);
        imv_facebook.setOnClickListener(this);

        imv_twitter = (ImageView)findViewById(R.id.imv_twitter);
        imv_twitter.setOnClickListener(this);

        imv_youtube = (ImageView)findViewById(R.id.imv_youTube);
        imv_youtube.setOnClickListener(this);

        imv_instagram = (ImageView)findViewById(R.id.imv_instagram);
        imv_instagram.setOnClickListener(this);

        imv_snap = (ImageView)findViewById(R.id.imv_snap);
        imv_snap.setOnClickListener(this);

        txv_send = (TextView)findViewById(R.id.txv_send);
        txv_send.setOnClickListener(this);

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.activity_contact_us);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_name.getWindowToken(), 0);
                return false;
            }
        });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                gotoMain();
                break;

            case R.id.txv_send:
                showToast("Send");
                break;

            case R.id.imv_facebook:

                showToast("facebook");
                break;

            case R.id.imv_twitter:
                showToast("twitter");
                break;

            case R.id.imv_youTube:
                showToast("youTube");
                break;

            case R.id.imv_instagram:
                showToast("instagram");
                break;

            case R.id.imv_snap:
                showToast("snapchat");
                break;
        }
    }

    private void gotoMain(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }
}
