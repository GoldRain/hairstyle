package com.sinmobile.gogorgeos.customcalendar.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.customcalendar.data.CalendarDate;
import com.sinmobile.gogorgeos.customcalendar.fragment.CalendarViewFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by joybar on 2/24/16.
 */
public class CalendarGridViewAdapter extends BaseAdapter {


    private   List<CalendarDate> mListData = new ArrayList<>();
    //ArrayList<Integer> _selectedDate = new ArrayList<>();


    public CalendarGridViewAdapter(List<CalendarDate> mListData) {
        this.mListData = mListData;
        //_selectedDate = Commons.selectedDate;
    }

    public List<CalendarDate> getListData() {
        return mListData;
    }

    public int getCount() {
        return mListData.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = null;

        final CalendarDate calendarDate = mListData.get(position);

        if (convertView == null) {

            viewHolder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.item_calendar, parent, false);

            viewHolder.tv_day = (TextView)convertView.findViewById(R.id.tv_day);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_day.setText(calendarDate.getSolar().solarDay+"");

        if(mListData.get(position).isInThisMonth()){

            if (Commons.MY_PROFILE == 1){

                if(calendarDate.isSelect()){

                    viewHolder.tv_day.setBackgroundResource(R.drawable.unselect_active_calendar_grid_item);

                }else {

                    viewHolder.tv_day.setBackgroundResource(R.drawable.selector_active_calendar_grid_item);

                }

                viewHolder.tv_day.setTextColor(Color.parseColor("#000000"));

            } else {

                if(calendarDate.isSelect()){

                    viewHolder.tv_day.setBackgroundResource(R.color.bg_color);

                }else {

                    viewHolder.tv_day.setBackgroundResource(R.color.white);

                }

                viewHolder.tv_day.setTextColor(Color.parseColor("#000000"));
            }


        } else {

            viewHolder.tv_day.setTextColor(Color.parseColor("#c1ecef"));
        }

        return convertView;
    }

    public static class ViewHolder {
        private TextView tv_day;
    }
}

