package com.sinmobile.gogorgeos.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.model.NewsModel;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/26/2017.
 */

public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsRecyclerViewAdapter.ImageHolder> {

    MainActivity _activity;
    ArrayList<NewsModel> _news = new ArrayList<>();
    View view;

    public NewsRecyclerViewAdapter(MainActivity activity, ArrayList<NewsModel> news){

        _activity = activity;
        _news = news;
        Log.d("newssize====", String.valueOf(_news.size()));
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news_recyle,viewGroup, false);
        ImageHolder viewHolder = new ImageHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageHolder imageHolder = (ImageHolder)v.getTag();
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImageHolder viewHolder, final int position) {

         final NewsModel news = _news.get(position);

        viewHolder.lyt_container.getLayoutParams().width = Commons.screenwidth/3;
        int height = Commons.screenwidth/3;
        viewHolder.lyt_container.getLayoutParams().height = height*5/4;

        viewHolder.imv_photo.getLayoutParams().width = height;
        Glide.with(_activity).load(news.get_news_imageurl()).placeholder(R.color.news_greay).into(viewHolder.imv_photo);
        viewHolder.txv_commnet.setText(news.get_news_content());
        viewHolder.lyt_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.NewsDetailsFragment(news);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null!= _news ? _news.size():0);
    }

    public class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imv_photo;
        TextView txv_commnet;
        LinearLayout lyt_container;

        public ImageHolder(View itemView) {
            super(itemView);

            lyt_container = (LinearLayout)view.findViewById(R.id.lyt_container);
            imv_photo = (ImageView) view.findViewById(R.id.imv_newsImage1);
            txv_commnet = (TextView)view.findViewById(R.id.txv_newsCom1);

        }
    }
}
