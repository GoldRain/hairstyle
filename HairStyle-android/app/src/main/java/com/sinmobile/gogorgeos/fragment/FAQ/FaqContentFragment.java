package com.sinmobile.gogorgeos.fragment.FAQ;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.FAQActivity;
import com.sinmobile.gogorgeos.model.FaqModel;


public class FaqContentFragment extends Fragment {

    FaqModel _faqModel;
    FAQActivity _activity;
    TextView ui_txvContent, ui_txvTitle;
    ImageView ui_imvBack;
    View view;

    public FaqContentFragment(FaqModel faqModel) {
        // Required empty public constructor

        _faqModel = faqModel;
        _activity = (FAQActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_faq_content, container, false);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        ui_imvBack = (ImageView) _activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.onBackPressed();
            }
        });


        ui_txvTitle = (TextView)view.findViewById(R.id.txv_title);
        ui_txvTitle.setText(_faqModel.get_title());

        ui_txvContent = (TextView)view.findViewById(R.id.txv_content);
        ui_txvContent.setText(_faqModel.get_content());

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (FAQActivity) context;
    }

}
