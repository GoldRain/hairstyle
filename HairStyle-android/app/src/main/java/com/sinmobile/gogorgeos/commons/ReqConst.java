package com.sinmobile.gogorgeos.commons;

import android.os.Handler;

/**
 * Created by GoldRain on 9/25/2016.
 */
public class ReqConst {

    public static final String SERVER_ADDR = "http://35.166.129.141";
    //35.166.129.141/index.php/Api
    //public static final String SERVER_ADDR = "http://192.168.1.120:1300/HairStyle";

    public static final String SERVER_URL = SERVER_ADDR + "/index.php/api/";


                    //request value

    public static final String REQ_LOGIN = "login";
    public static final String REQ_SENDVERI_CODE = "sendVerifyCode";
    public static final String REQ_CHANGEPASSWORD = "changePassword";
    public static final String REQ_REGISTER_USER = "registerUser";
    public static final String REQ_GETHOMEDATA = "getHomeData";
    public static final String REQ_GETUSERS = "getUsers";
    public static final String REQ_UPDATEPROFILE = "updateProfile";
    public static final String REQ_UPLOADIMAGGE = "uploadImage";
    public static final String REQ_UPLOADVIDEO1 = "uploadVideo1";
    public static final String REQ_UPLOADVIDEO2 = "uploadVideo2";
    public static final String REQ_WRITEREVIEW = "writeReview";
    public static final String REQ_GETTRAINING = "getTraining";
    public static final String REQ_GETNEWS = "getNews";
    public static final String RES_USERS = "users";
    public static final String RES_GETTRAINING = "getTraining";
    public static final String REQ_SEARCH_NANE = "searchUserByName";
    public static final String REQ_SEARCH_DATE = "searchUserByAvailable";
    public static final String REQ_SEARCH_ADDRESS = "searchUserByAddress";
    public static final String REQ_GETALLFAQ = "getAllFaq";
    public static final String REQ_GETABOUTUS = "getAboutUS";
    public static final String REQ_GETCONTACTUS = "getContactUS";

    public static final String REQ_SETAVAILABLE = "setAvailable";


                     //request params

    /*REGISTER*/
    public static final String PARAM_ID = "id";
    public static final String PARAM_USERID = "user_id";
    public static final String PARAM_USEREMAIL = "user_email";
    public static final String PARAM_USER_FIRSTNAME = "user_firstname";
    public static final String PARAM_USER_LASTNAME = "user_lastname";
    public static final String PARAM_USER_ADDRESS = "user_address";
    public static final String PARAM_USER_SEVERLOCATION = "user_servelocation";
    public static final String PARAM_USERPWD = "user_password";
    public static final String PARAM_USERGENDER = "user_gender";
    public static final String PARAM_USER_BIRTHDAY = "user_birthday";
    public static final String PARAM_USER_SUBSCRIBED = "user_subscribed";
    public static final String PARAM_USERROLE = "user_role";
    public static final String PARAM_USER_PHONENUMBER = "user_phonenumber";
    public static final String PARAM_USER_PHOTOURL = "user_photourl";
    public static final String PARAM_USER_IMAGEURL = "images_url";
    public static final String PARAM_FBLINK = "user_fblink";
    public static final String PARAM_TWLINK = "user_twlink";
    public static final String PARAM_YOLINK = "user_yolink";
    public static final String PARAM_SNLINK = "user_snlink";
    public static final String PARAM_INLINK = "user_inlink";
    public static final String PARAM_ABOUTME = "user_aboutme";
    public static final String PARAM_USER_VERICODE = "user_verificationcode";
    public static final String RARAM_YEAR = "year";
    public static final String RARAM_MONTH = "month";
    public static final String RARAM_VALUE = "value";
    public static final String PARAM_VIDEO_ID = "video_id";
    /*GETHOMEDATA*/
    public static final String PARAM_USER_LANGUAGE = "user_language";
    /*NEWS*/
    public static final String PARAM_NEWSID = "news_id";
    public static final String PARAM_NEWSTITLE = "news_title";
    public static final String PARAM_NEWSDATE = "news_date";
    public static final String PARAM_NEWSIMAGEURL = "news_imageurl";
    public static final String PARAM_NEWSCONTENT = "news_content";
    /*TRAININGS*/
    public static final String PARAM_TRAINID = "training_id";
    public static final String PARAM_TRAINTITLE = "training_title";
    public static final String PARAM_TRAINDATE = "training_date";
    public static final String PARAM_TRAINLOCATION = "training_location";
    public static final String PARAM_TRAINEMAIL = "training_email";
    public static final String PARAM_TRAINIMAGEURL = "training_imageurl";
    public static final String PARAM_TRAINCONTENT = "training_content";
    public static final String PARAM_TRAINPRICE = "training_price";
    public static final String PARAM_TRAIN_PHONENUMBER = "training_phonenumber";
    /*UPDATEPROFILE*/
    public static final String PARAM_USER_ID = "user_id";
    public static final String REQ_PARAM_FILE = "user_photo";
    public static final String PARAM_USERPHOTO = "user_photo";
    /*UPLOADVIDEO*/
    public static final String PARAM_VIDEO_THUMBNAILIMAGE = "video_thumbnailimage";
    public static final String PARAM_VIDEO_VIDEO = "video_video";
    public static final String PARAM_VIDEO_COMMENT = "video_comment";
    public static final String PARAM_VIDEO_THUMBNAILURL = "video_thumbimageurl";
    public static final String PARAM_VIDEO_URL = "video_url";
    /*WRITEREVIEW*/
    public static final String PARAM_RATESENDER = "rate_sender";
    public static final String PARAM_RECEIVER = "rate_receiver";
    public static final String PARAM_RATEMARKS = "rate_marks";
    public static final String PARAM_RATECOMMENT = "rate_comment";
    /*SEARCH*/
    public static final String PARAM_NAME = "name";
    public static final String PARAM_YEAR = "year";
    public static final String PARAM_MONTH = "month";
    public static final String PARAM_VALUE = "value";
    public static final String PARAM_ADDRESS = "address";
    /*FAQ*/
    public static final String PARAM_FAQID = "faq_id";
    public static final String PARAM_FAQTITLE = "faq_title";
    public static final String PARAM_FAQCONTENT = "faq_content";
    /*ABOUT US*/
    public static final String PARAM_ABOUTUS_ID = "aboutus_id";
    public static final String PARAM_ABOUTUSTITLE = "aboutus_title";
    public static final String PARAM_ABOUTUS_CONTENT = "aboutus_content";
    /*CONTACT US*/
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PHONENUMBER = "phonenumber";

    /*SET AVAILABLE*/
    public static final String PARAM_AVAILABLE = "user_available";



                    //response value

    /*LOGIN*/
    public static final String RES_USERINFO = "user_info";
    public static final String RES_USERID = "user_id";
    public static final String RES_USER_FIRSTNAME = "user_firstname";
    public static final String RES_USER_LASTNAME = "user_lastname";
    public static final String RES_USEREMAIL = "user_email";
    public static final String RES_USERGENDER = "user_gender";
    public static final String RES_USER_BIRTHDAY = "user_birthday";
    public static final String RES_USER_SUBSCRIBED = "user_subscribed";
    public static final String RES_USERROLE = "user_role";
    public static final String RES_USER_PHONENUMBER = "user_phonenumber";
    public static final String RES_USERADDRESS = "user_address";
    public static final String RES_USERPHOTOURL = "user_photourl";
    public static final String RES_USERIMAGES = "user_images";
    public static final String RES_USER_AVAILABLE = "user_available";
    /*PROFILE*/
    public static final String RES_USERVIDEOS = "user_videos";
    public static final String RES_VIDEOID = "video_id";
    public static final String RES_VIDEO_URL = "video_url";
    public static final String RES_THUMB_IMAGEURL = "video_thumbimageurl";
    public static final String RES_VIDEO_COMMENT = "video_comment";
    public static final String RES_USER_SOCIALLINK = "user_sociallink";
    public static final String RES_USERRATINGS = "user_ratings";
    public static final String RES_RATEID = "rate_id";
    public static final String RES_RATESENDER = "rate_sender";
    public static final String RES_RATESENDER_PHOTOURL = "rate_senderphoto";
    public static final String RES_RATESENDERNAME = "rate_sendername";
    public static final String RES_RATERECEIVER = "rate_receiver";
    public static final String RES_RATEMARKS = "rate_marks";
    public static final String RES_RATEREVIEW = "rate_review";
    /*GET HOMEDATA*/
    public static final String RES_HOMEINFO = "homeinfo";
    public static final String RES_ADVERTISEMENT = "advertisement";
    public static final String RES_ARTISTS = "artists";
    public static final String RES_STYLISTS = "stylists";
    public static final String RES_MODELS = "models";
    public static final String RES_NEWS = "news";
    public static final String RES_TRAININGS = "trainings";
    public static final String RES_USER = "user";
    /*WRITEREVIEW*/
    public static final String RES_ID = "id";

    public static final String RES_ADDRESS = "address";
    public static final String RES_FAQINFO = "faq_info";
    public static final String RES_ABOUTUSINFO = "aboutus_info";
    public static final String RES_CONTACTUSINFO = "contactus_info";

    /*Register*/
    /*RESULT_CODE*/
    public static final String RES_MESSAGE = "message";
    public static final String CODE_SUCCESS = "Success";

}
