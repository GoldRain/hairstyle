package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.ImagePreviewActivity;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.customview.SquareImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/20/2017.
 */

public class ImageGridViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<String> _images = new ArrayList<>();
    ArrayList<String> _allImage = new ArrayList<>();

    public ImageGridViewAdapter (MainActivity activity){

        _activity = activity;

    }

    public void setImages(ArrayList<String> image){

        _allImage = image;
        _images.clear();
        _images.addAll(_allImage);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return _images.size();
    }

    @Override
    public Object getItem(int position) {
        //return _images.get(position);

        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ImageHolder imageHolder;

        if (convertView == null){

            imageHolder = new ImageHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.item_image_grid, parent, false );

            imageHolder.imv_image = (SquareImageView) convertView.findViewById(R.id.imv_image);

          //  imageHolder.imv_image.setImageResource(_images.get(position));

            convertView.setTag(imageHolder);

        } else {

            imageHolder = (ImageHolder)convertView.getTag();
        }

        final String image = (String) _images.get(position);
        /*Log.d("image++USer", image);*/

        Glide.with(_activity).load(image).placeholder(R.drawable.bg_non_profile).into(imageHolder.imv_image);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(_activity, ImagePreviewActivity.class);
                intent.putExtra(Constants.KEY_IMAGEPATH, _allImage);
                intent.putExtra(Constants.KEY_POSITION, position);
                _activity.startActivity(intent);

            }
        });

        return convertView;
    }

    public class ImageHolder {

        SquareImageView imv_image;
    }
}
