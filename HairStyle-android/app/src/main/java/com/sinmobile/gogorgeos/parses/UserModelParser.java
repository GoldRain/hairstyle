package com.sinmobile.gogorgeos.parses;

import android.util.Log;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.LoginActivity;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.AvailableModel;
import com.sinmobile.gogorgeos.model.RatingModel;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.model.VideoModel;
import com.sinmobile.gogorgeos.preference.PrefConst;
import com.sinmobile.gogorgeos.preference.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 2/6/2017.
 */

public class UserModelParser extends CommonActivity{

    UserModel _user;

    ArrayList<String> _user_images = new ArrayList<>();
    ArrayList<VideoModel> _user_videos = new ArrayList<>();
    ArrayList<RatingModel> _user_ratings = new ArrayList<>();
    ArrayList<AvailableModel> _user_available = new ArrayList<>();

    boolean _isFinish = false;


    public void parseJson(JSONObject object) {

        try {

            _user = new UserModel();

            JSONObject user = object.getJSONObject(ReqConst.RES_USERINFO);

            _user.set_user_id(user.getInt(ReqConst.RES_USERID));
            _user.set_user_firstname(user.getString(ReqConst.PARAM_USER_FIRSTNAME));
            _user.set_user_lastname(user.getString(ReqConst.PARAM_USER_LASTNAME));
            _user.set_user_email(user.getString(ReqConst.PARAM_USEREMAIL));
            _user.set_user_gender(user.getInt(ReqConst.PARAM_USERGENDER));
            _user.set_user_birthday(user.getString(ReqConst.PARAM_USER_BIRTHDAY));
            _user.set_user_subscribed(user.getInt(ReqConst.PARAM_USER_SUBSCRIBED));
            _user.set_user_role(user.getInt(ReqConst.PARAM_USERROLE));
            _user.set_user_phonenumber(user.getString(ReqConst.PARAM_USER_PHONENUMBER));
            _user.set_user_adress(user.getString(ReqConst.PARAM_USER_ADDRESS));
            _user.set_serve_location(user.getString(ReqConst.PARAM_USER_SEVERLOCATION));
            _user.set_user_photoUrl(user.getString(ReqConst.PARAM_USER_PHOTOURL));
            _user.set_user_fblink(user.getString(ReqConst.PARAM_FBLINK));
            _user.set_user_twlink(user.getString(ReqConst.PARAM_TWLINK));
            _user.set_user_yolink(user.getString(ReqConst.PARAM_YOLINK));
            _user.set_user_inlink(user.getString(ReqConst.PARAM_INLINK));
            _user.set_user_snlink(user.getString(ReqConst.PARAM_SNLINK));
            _user.set_user_aboutme(user.getString(ReqConst.PARAM_ABOUTME));


                /*USER_IMAGES*/
            JSONArray user_images = user.getJSONArray(ReqConst.RES_USERIMAGES);
            for (int i = 0; i < user_images.length(); i++){

                JSONObject jsonImage = (JSONObject) user_images.get(i);

                _user_images.add(jsonImage.getString(ReqConst.PARAM_USER_IMAGEURL));
            }
            _user.set_user_images(_user_images);

                /*USER_VIDEOS*/
            JSONArray user_videos = user.getJSONArray(ReqConst.RES_USERVIDEOS);
            for (int i = 0; i < user_videos.length(); i++){

                JSONObject jsonVideos = (JSONObject)user_videos.get(i);

                VideoModel video = new VideoModel();

                video.set_video_id(jsonVideos.getInt(ReqConst.RES_VIDEOID));
                video.set_video_url(jsonVideos.getString(ReqConst.RES_VIDEO_URL));
                video.set_video_thumbimageurl(jsonVideos.getString(ReqConst.RES_THUMB_IMAGEURL));
                video.set_video_comment(jsonVideos.getString(ReqConst.RES_VIDEO_COMMENT));

                _user_videos.add(video);
            }
            _user.set_user_video(_user_videos);

                /*USER_RATINGS*/
            JSONArray user_ratings = user.getJSONArray(ReqConst.RES_USERRATINGS);
            for (int i = 0; i < user_ratings.length(); i++ ){

                JSONObject jsonRatings = (JSONObject) user_ratings.get(i);
                RatingModel _rating = new RatingModel();

                _rating.set_rate_id(jsonRatings.getInt(ReqConst.RES_RATEID));
                _rating.set_rate_sender(jsonRatings.getInt(ReqConst.RES_RATESENDER));
                _rating.set_rate_senderphoto(jsonRatings.getString(ReqConst.RES_RATESENDER_PHOTOURL));
                _rating.set_rate_sendername(jsonRatings.getString(ReqConst.RES_RATESENDERNAME));
                _rating.set_rate_receiver(jsonRatings.getInt(ReqConst.RES_RATERECEIVER));
                _rating.set_rate_marks(jsonRatings.getInt(ReqConst.RES_RATEMARKS));
                _rating.set_rate_comment(jsonRatings.getString(ReqConst.RES_RATEREVIEW));

                _user_ratings.add(_rating);
            }

            _user.set_user_ratings(_user_ratings);

            JSONArray user_available = user.getJSONArray(ReqConst.RES_USER_AVAILABLE);
            for (int i = 0; i < user_available.length(); i++){

                JSONObject jsonAvailable = (JSONObject)user_available.get(i);

                AvailableModel available = new AvailableModel();

                available.set_year(jsonAvailable.getInt(ReqConst.RARAM_YEAR));
                available.set_month(jsonAvailable.getInt(ReqConst.RARAM_MONTH));
                available.set_value(jsonAvailable.getLong(ReqConst.RARAM_VALUE));

                _user_available.add(available);
            }
            _user.set_user_available(_user_available);

            _isFinish = true;

        }catch (JSONException e){

        }
    }

    public boolean is_isFinish() {
        return _isFinish;
    }

    public UserModel get_user() {
        return _user;
    }
}
