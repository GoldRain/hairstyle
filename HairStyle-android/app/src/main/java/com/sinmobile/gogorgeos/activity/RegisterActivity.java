package com.sinmobile.gogorgeos.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.adapter.GenderSpinnerAdapter;
import com.sinmobile.gogorgeos.adapter.RoleSpinnerAdapter;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    private int _id = 0;
    EditText ui_edtFirstName, ui_edtLastName, ui_edtEmail, ui_edtPassword, ui_edtConfirmPwd;
    CheckBox ui_checkNews, ui_checkTerm;
    int _checkedSub = 0;
    int _checkedTerm = 0;
    LinearLayout ui_lytNews, ui_lytAccept;
    TextView ui_txvDate,ui_txvRegister, txv_accept;

    Spinner spinner_role;
    RoleSpinnerAdapter _adapter_role;

    Spinner spinner_gender;
    GenderSpinnerAdapter _adapter_gender;

    public static boolean _register = false;
    ForgetActivity forgetActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView) findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_edtFirstName = (EditText)findViewById(R.id.edt_firstName);
        ui_edtLastName = (EditText)findViewById(R.id.edt_lastName);
        ui_edtEmail = (EditText)findViewById(R.id.edt_email);
        ui_edtPassword = (EditText)findViewById(R.id.edt_password);
        ui_edtConfirmPwd = (EditText)findViewById(R.id.edt_confirm_pwd);

        /*role_spinner*/
        spinner_role = (Spinner) findViewById(R.id.spinner_role);
        _adapter_role = new RoleSpinnerAdapter(this);
        spinner_role.setAdapter(_adapter_role);
        //////////////////////////////////

        /*Gender_spinner*/
        spinner_gender = (Spinner) findViewById(R.id.spinner_gender);
        _adapter_gender = new GenderSpinnerAdapter(this);
        spinner_gender.setAdapter(_adapter_gender);
        //////////////////////////////////

        ui_txvDate = (TextView)findViewById(R.id.txv_date);
        ui_txvDate.setOnClickListener(this);

        ui_txvRegister = (TextView)findViewById(R.id.txv_register);
        ui_txvRegister.setOnClickListener(this);

        ui_lytAccept = (LinearLayout)findViewById(R.id.lyt_accept);
        ui_lytAccept.setOnClickListener(this);

        ui_lytNews = (LinearLayout)findViewById(R.id.lyt_news);
        ui_lytNews.setOnClickListener(this);

        ui_checkNews = (CheckBox)findViewById(R.id.checkbox_accept);
        ui_checkTerm = (CheckBox)findViewById(R.id.checkbox_news);

        // container
        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.activity_register);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtFirstName.getWindowToken(), 0);
                return false;
            }
        });

    }
    public boolean checkValid() {

        if (ui_edtFirstName.getText().toString().length() == 0) {
            showAlertDialog(getString(R.string.input_firstname));
            return false;

        } else if (ui_edtLastName.getText().toString().length() == 0) {
            showAlertDialog(getString(R.string.input_lastname));
            return false;

        } else if (ui_edtEmail.getText().toString().length() == 0 ){

            showAlertDialog(getString(R.string.input_email));
            return false;

        } else if (!Patterns.EMAIL_ADDRESS.matcher(ui_edtEmail.getText().toString()).matches()){

            showAlertDialog(getString(R.string.input_correct_email));
            return false;

        }  else if (ui_edtPassword.getText().toString().length() == 0) {
            showAlertDialog(getString(R.string.input_pwd));
            return false;

        } else if (!ui_edtPassword.getText().toString().equals(ui_edtConfirmPwd.getText().toString())){

            showAlertDialog(getString(R.string.input_corect_pwd));
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                gotoLogin();
                break;

            case R.id.txv_date:

                showDatePickerDialog(v);
                break;

            case R.id.lyt_accept:

                gotoTerms();
                break;

            case R.id.lyt_news:

                break;

            case R.id.txv_register:

                if (checkValid()){
                    _register = true;
                    setRegister();
                    progressVerification();
                }

                break;

            case R.id.checkbox_news:

                if (ui_checkNews.isSelected()){
                ui_checkNews.setChecked(true);
                _checkedSub = 1;}

                else {
                    ui_checkNews.setChecked(false);
                    _checkedSub = 0;
                }
                break;

            case R.id.checkbox_accept:

                if (ui_checkTerm.isSelected()){
                    ui_checkTerm.setChecked(true);
                    _checkedTerm = 1;

                }else {
                    ui_checkTerm.setChecked(false);
                    _checkedTerm = 0;
                }
                break;
        }

    }

    private void gotoTerms(){

        startActivity(new Intent(this, TermsActivity.class));
        overridePendingTransition(0,0);
    }

    private void progressVerification() {

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SENDVERI_CODE;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseVerifyCode(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_USEREMAIL, ui_edtEmail.getText().toString());

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseVerifyCode(String response){

        try {
            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);
            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                closeProgress();
                showToast(result_message);

                gotoVerification();

            } else {

                closeProgress();
                showAlertDialog(result_message);
            }
        } catch (JSONException e) {

            closeProgress();
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }

    }

    private void setRegister(){

        UserModel user = new UserModel();

        user.set_user_firstname(ui_edtFirstName.getText().toString());
        user.set_user_lastname(ui_edtLastName.getText().toString());
        user.set_user_role(spinner_role.getSelectedItemPosition());
        user.set_user_gender(spinner_gender.getSelectedItemPosition());
        user.set_user_birthday(ui_txvDate.getText().toString());
        user.set_user_email(ui_edtEmail.getText().toString());
        user.set_user_password(ui_edtPassword.getText().toString());
        user.set_user_subscribed(_checkedSub);

        Commons.REGISTER_USER = user;
    }

    private void gotoLogin(){

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0,0);
    }

    private void gotoVerification(){

        forgetActivity._forget = false;
        Intent intent = new Intent(this, VerificationActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0,0);
    }

     /*TimePicker*//////////////////////////////////////

    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            String date = String.valueOf(year) + "-" + String.valueOf(month) + "-" + String.valueOf(day);
            ui_txvDate.setText(date);

            //Commons.userprofile.set_user_birthday(date);
            //year1 = String.valueOf(year)+ "/" + String.valueOf(month) + "/" + String.valueOf(day);
        }
    }

    public void showDatePickerDialog(View v) {

        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void onBackPressed() {
        gotoLogin();
        overridePendingTransition(0,0);
    }
}
