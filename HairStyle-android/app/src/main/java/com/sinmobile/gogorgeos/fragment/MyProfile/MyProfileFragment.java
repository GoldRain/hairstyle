package com.sinmobile.gogorgeos.fragment.MyProfile;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.MyProfileViewPagerAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.fragment.MyAlertDialogFragment;
import com.sinmobile.gogorgeos.model.RatingModel;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.model.VideoModel;
import com.sinmobile.gogorgeos.utils.RadiusImageView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static com.sinmobile.gogorgeos.commons.Commons.YEAR;


public class MyProfileFragment extends Fragment implements View.OnClickListener {

    MainActivity _activity;
    View view;
    TabLayout tabLayout;
    ViewPager viewPager;
    MyProfileViewPagerAdapter _adapter;
    RadiusImageView ui_imvPhoto;

    ImageView ui_imvBack, imv_edit, imv_calendar;
    TextView txv_name, txv_location, txv_serve_location, txv_phoneNum, txv_email,txv_my_about;
    ImageView imv_facebook, imv_twitter, imv_youtube, imv_instagram, imv_snap;

    static  UserModel myProfile = new UserModel();

    private boolean isChoiceModelSingle = false;
    List<Long> _valuesArray = new ArrayList<>();

    public MyProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view =  inflater.inflate(R.layout.fragment_my_profile, container, false);

        loadlayout();
        return view;
    }

    private void loadlayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.onBackPressed();
            }
        });

        imv_calendar = (ImageView)_activity.findViewById(R.id.imv_calendar);
        imv_calendar.setOnClickListener(this);

        ui_imvPhoto = (RadiusImageView)view.findViewById(R.id.imv_photo);
        ui_imvPhoto.setOnClickListener(this);

        imv_edit = (ImageView)view.findViewById(R.id.imv_edit);
        imv_edit.setOnClickListener(this);

        txv_name = (TextView)view.findViewById(R.id.txv_name);
        txv_location = (TextView)view.findViewById(R.id.txv_location);
        txv_serve_location = (TextView)view.findViewById(R.id.txv_sever_location);
        txv_phoneNum = (TextView)view.findViewById(R.id.txv_phone_num);
        txv_email = (TextView)view.findViewById(R.id.txv_email);
        txv_my_about = (TextView)view.findViewById(R.id.txv_my_about);

        imv_facebook = (ImageView)view.findViewById(R.id.imv_facebook);
        imv_facebook.setOnClickListener(this);

        imv_twitter = (ImageView)view.findViewById(R.id.imv_twitter);
        imv_twitter.setOnClickListener(this);

        imv_youtube = (ImageView)view.findViewById(R.id.imv_youTube);
        imv_youtube.setOnClickListener(this);

        imv_instagram = (ImageView)view.findViewById(R.id.imv_instagram);
        imv_instagram.setOnClickListener(this);

        imv_snap = (ImageView)view.findViewById(R.id.imv_snap);
        imv_snap.setOnClickListener(this);

        FragmentManager manager = _activity.getSupportFragmentManager();
        _adapter = new MyProfileViewPagerAdapter(manager, _activity);
        viewPager = (ViewPager)view.findViewById(R.id.viewpager);
        viewPager.setAdapter(_adapter);

        tabLayout = (TabLayout)view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupTabIcons();

        if (Commons.g_user != null){
        showProfile();
        }

        // ================== All Data===============================
        //Commons.myprofile = new UserModel();

        //===================== My profile Api Call=====================================

    }

    private void showProfile(){

        //UserModel myProfile = new UserModel();
        myProfile = Commons.g_user;

        Glide.with(_activity).load(myProfile.get_user_photoUrl()).placeholder(R.drawable.bg_non_profile).into(ui_imvPhoto);
        txv_name.setText(myProfile.get_user_fullname());
        txv_location.setText(myProfile.get_user_adress());
        txv_serve_location.setText(myProfile.get_serve_location());
        txv_phoneNum.setText(myProfile.get_user_phonenumber());
        txv_email.setText(myProfile.get_user_email());
        txv_my_about.setText(myProfile.get_user_aboutme());

    }

    public void setupTabIcons(){

        int[] tabIcons = { R.drawable.image_frag, R.drawable.video_frag, R.drawable.review_frag };

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_edit:
                _activity.MyProfileEditFragment();
                break;

            case R.id.imv_calendar:
                view_calendar_my();
                break;

            case R.id.imv_facebook:
                _activity.showToast("Facebook");
                break;

            case R.id.imv_twitter:
                break;

            case R.id.imv_youTube:
                break;

            case R.id.imv_instagram:
                _activity.showToast("Instagram");
                break;

            case R.id.imv_snap:
                break;

        }
    }

    private void view_calendar_my(){

        if (Commons.g_user.get_user_available().size() == 0){

            _valuesArray = (Commons.AVAILABLE_YEAR);

        } else {

            for (int i = 0; i < 12; i++){

                long value =  Commons.g_user.get_user_available().get(i).get_value();

                _valuesArray.add(value);
            }
        }

        MyAlertDialogFragment dialogFragment = MyAlertDialogFragment.newInstance(isChoiceModelSingle, _valuesArray);

        dialogFragment.show(getFragmentManager().beginTransaction(), "DialogFragment");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }
}
