package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;

import java.util.ArrayList;

/**
 * Created by HugeRain on 2/18/2017.
 */

public class LocationListAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<String> _locations = new ArrayList<>();
    ArrayList<String> _allLocation = new ArrayList<>();

    public LocationListAdapter(MainActivity activity){

        _activity = activity;
    }

    public void setLocations(ArrayList<String> locations){

        _allLocation = locations;
        _locations.clear();
        _locations.addAll(_allLocation);

        notifyDataSetChanged();

    }

    @Override
    public int getCount() {
        return _locations.size();
    }

    @Override
    public Object getItem(int position) {
        return _locations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LocationHolder locationHolder;

        if (convertView == null){

            locationHolder = new LocationHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_location_list, parent, false);

            locationHolder.txv_location = (TextView)convertView.findViewById(R.id.txv_location);

            convertView.setTag(locationHolder);

        } else {

            locationHolder = (LocationHolder)convertView.getTag();
        }

        final String location = _locations.get(position);

        locationHolder.txv_location.setText(location);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.setSearchLocation(location);
                _locations.clear();
            }
        });

        return convertView;
    }

    public void filter(String charText){

        charText = charText.toLowerCase();

        _locations.clear();

        if(charText.length() == 0){

           _locations.clear();


        } else {

            for (String user : Commons.ADDRESS){

                if (user.contains(charText)) {

                    _locations.add(user);
                }

            }
        }
        notifyDataSetChanged();
    }

    public class LocationHolder {

        public TextView txv_location;
    }
}
