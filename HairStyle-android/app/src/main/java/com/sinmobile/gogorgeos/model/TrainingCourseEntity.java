package com.sinmobile.gogorgeos.model;

import java.io.Serializable;

/**
 * Created by ToSuccess on 1/21/2017.
 */

public class TrainingCourseEntity implements Serializable {

    int _id_training = 0;
    String _image_training = "";
    String _title_training = "";


    public int get_id_training() {
        return _id_training;
    }

    public void set_id_training(int _id_training) {
        this._id_training = _id_training;
    }

    public String get_image_training() {
        return _image_training;
    }

    public void set_image_training(String _image_training) {
        this._image_training = _image_training;
    }

    public String get_title_training() {
        return _title_training;
    }

    public void set_title_training(String _title_training) {
        this._title_training = _title_training;
    }
}
