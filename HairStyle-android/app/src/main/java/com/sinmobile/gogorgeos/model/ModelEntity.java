package com.sinmobile.gogorgeos.model;

import java.io.Serializable;

/**
 * Created by ToSuccess on 1/21/2017.
 */

public class ModelEntity implements Serializable {

    int _id_model  = 0;
    String _photo_model = "";
    String _name_model = "";


    public int get_id_model() {
        return _id_model;
    }

    public void set_id_model(int _id_model) {
        this._id_model = _id_model;
    }

    public String get_photo_model() {
        return _photo_model;
    }

    public void set_photo_model(String _photo_model) {
        this._photo_model = _photo_model;
    }

    public String get_name_model() {
        return _name_model;
    }

    public void set_name_model(String _name_model) {
        this._name_model = _name_model;
    }
}
