package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;

/**
 * Created by ToSuccess on 1/25/2017.
 */

public class LangSpinnerAdapter extends BaseAdapter{

    private final Context _context;

    String[] Language;

    public LangSpinnerAdapter(Context context){

        this._context = context;
        Language = _context.getResources().getStringArray(R.array.Lainguage);
    }

    @Override
    public int getCount() {
        return Language.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_lang_spin, parent, false);

        TextView ui_txv_lang = (TextView)convertView.findViewById(R.id.txv_lang);

     /*   if (position == 0){

            ui_txv_lang.setText(Language[0].trim());
            ui_txv_lang.setTextColor(_context.getResources().getColor(R.color.hint_color));
        } else {
            ui_txv_lang.setText(Language[position].trim());
        }
*/
        ui_txv_lang.setText(Language[position].trim());
        return convertView;
    }
}
