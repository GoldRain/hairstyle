package com.sinmobile.gogorgeos.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;

import java.io.InputStream;

public class AboutUsFragment extends Fragment implements View.OnClickListener {

    MainActivity _activity;
    View view;
    ImageView ui_imvBack;
    ScrollView ui_scrView;
    TextView txv_title, ui_txvAboutUs;
    ImageView imv_facebook, imv_twitter, imv_youtube, imv_instagram, imv_snap;

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_about_us, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.onBackPressed();
            }
        });

        ui_scrView = (ScrollView)view.findViewById(R.id.scv_aboutUs);

        txv_title = (TextView)view.findViewById(R.id.txv_title);
        txv_title.setText(Commons.ABOUTUS_TITLE);

        ui_txvAboutUs = (TextView)view.findViewById(R.id.txv_aboutUs_content);
        ui_txvAboutUs.setText(Commons.ABOUTUS_CONTENT);

        imv_facebook = (ImageView)view.findViewById(R.id.imv_facebook);
        imv_facebook.setOnClickListener(this);

        imv_twitter = (ImageView)view.findViewById(R.id.imv_twitter);
        imv_twitter.setOnClickListener(this);

        imv_youtube = (ImageView)view.findViewById(R.id.imv_youTube);
        imv_youtube.setOnClickListener(this);

        imv_instagram = (ImageView)view.findViewById(R.id.imv_instagram);
        imv_instagram.setOnClickListener(this);

        imv_snap = (ImageView)view.findViewById(R.id.imv_snap);
        imv_snap.setOnClickListener(this);
/*
        String result;
        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.terms);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);
        } catch (Exception e) {
            result = "Error : can't show file."  ;
        }

        ui_txvAboutUs.setText(result.toString());*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_facebook:
                _activity.showToast("facebook");
                break;

            case R.id.imv_twitter:
                break;

            case R.id.imv_youTube:
                break;

            case R.id.imv_instagram:
                break;

            case R.id.imv_snap:
                _activity.showToast("snap");
                break;
        }
    }
}
