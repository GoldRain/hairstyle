package com.sinmobile.gogorgeos.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.AvailableModel;
import com.sinmobile.gogorgeos.model.RatingModel;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.model.VideoModel;
import com.sinmobile.gogorgeos.parses.UserModelParser;
import com.sinmobile.gogorgeos.preference.PrefConst;
import com.sinmobile.gogorgeos.preference.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    EditText ui_edtEmail, ui_edtPassword;
    TextView ui_txvLogin, ui_txvRegister, ui_txvForget;


    String _email = "";
    String _pwd = "";

    UserModel _user = new UserModel();
    ArrayList<String> _user_images = new ArrayList<>();
    ArrayList<VideoModel> _user_videos = new ArrayList<>();
    ArrayList<RatingModel> _user_ratings = new ArrayList<>();
    ArrayList<AvailableModel> _user_available = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initValue();
        loadLayout();
    }

    boolean _isFromLogout = false;
    private void initValue() {

        Intent intent = getIntent();
        try{
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);
        } catch (Exception e){}
    }

    private void loadLayout() {

        ui_imvBack = (ImageView) findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_edtEmail = (EditText)findViewById(R.id.edt_email);
        ui_edtPassword = (EditText)findViewById(R.id.edt_password);

        ui_txvLogin = (TextView)findViewById(R.id.txv_login);
        ui_txvLogin.setOnClickListener(this);

        ui_txvRegister = (TextView)findViewById(R.id.txv_register);
        ui_txvRegister.setOnClickListener(this);

        ui_txvForget = (TextView)findViewById(R.id.txv_forget);
        ui_txvForget.setOnClickListener(this);

        // container
        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.activity_login);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtEmail.getWindowToken(), 0);
                return false;
            }
        });

        if(_isFromLogout){

            //save user to empty.
            Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, "");
            Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, "");

            ui_edtEmail.setText("");
            ui_edtPassword.setText("");

        } else {
            String email = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USEREMAIL, "");
            String pwd = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERPWD, "");

            if (email.length() > 0 && pwd.length() > 0) {

                _email = email;
                _pwd = pwd;

                ui_edtEmail.setText(_email);
                ui_edtPassword.setText(_pwd);

                //gotoLogin();
            }
        }
    }

    private void gotoLogin(){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseLogin(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_USEREMAIL, ui_edtEmail.getText().toString());
                    params.put(ReqConst.PARAM_USERPWD, ui_edtPassword.getText().toString());

                } catch (Exception e) {

                    closeProgress();
                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseLogin(String json){

        Log.d("====ARTISTS===>", json);

        try {
            JSONObject object = new JSONObject(json);

            String result_message = object.getString(ReqConst.RES_MESSAGE);

            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                closeProgress();

                UserModelParser modelParser = new UserModelParser();
                modelParser.parseJson(object);

                while (!modelParser.is_isFinish()){}

                Commons.g_user =  modelParser.get_user();
                Commons.g_user.set_user_password(_pwd);


                Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, _pwd);

                Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, _email);

                String lastLoginEmail = Preference.getInstance().getValue(getApplicationContext(), PrefConst.PREFKEY_LASTLOGINID, "");

                // init database if new user login
                if (!lastLoginEmail.equals(_email)) {
                    Preference.getInstance().put(getApplicationContext(), PrefConst.PREFKEY_LASTLOGINID, _email);
                }

                gotoMain();

            }else  {

                showAlertDialog(result_message);
                closeProgress();
            }

        } catch (JSONException e) {
            closeProgress();
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
        }


    }

    private boolean checkValue(){

        if (ui_edtEmail.getText().length() == 0){

            showAlertDialog(getString(R.string.input_email));
            return false;

        } else if (ui_edtPassword.getText().length() == 0){

            showAlertDialog(getString(R.string.input_password));
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                gotoMain();
                break;

            case R.id.txv_login:
                if (checkValue()) {

                    _email = ui_edtEmail.getText().toString().trim();
                    _pwd = ui_edtPassword.getText().toString().trim();

                    gotoLogin();
                }
                break;

            case R.id.txv_register:
                gotoRegister();
                break;

            case R.id.txv_forget:
                gotoForget();
                break;
        }
    }

    private void gotoMain(){

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0,0);
    }

    private void gotoRegister(){

        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0,0);
    }

    private void gotoForget(){

        Intent intent = new Intent(this, ForgetActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0,0);
    }

    @Override
    public void onBackPressed() {
        gotoMain();
        overridePendingTransition(0,0);
    }
}
