package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.model.NewsModel;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/22/2017.
 */

public class NewsGridViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<NewsModel> _newsModels = new ArrayList<>();


    public NewsGridViewAdapter(MainActivity activity, ArrayList<NewsModel> newsModels){

        _activity = activity;
        _newsModels = newsModels;
    }

    @Override
    public int getCount() {
        return _newsModels.size();
    }

    @Override
    public Object getItem(int position) {
        return _newsModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final NewsHolder newsHolder;

        if (convertView == null){

            newsHolder = new NewsHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_news_gdv, parent, false);

            newsHolder.imv_newsImage = (ImageView)convertView.findViewById(R.id.imv_newsImage);
            newsHolder.txv_newsContent = (TextView)convertView.findViewById(R.id.txv_newsContent);
            newsHolder.imv_view = (ImageView)convertView.findViewById(R.id.imv_view);
            newsHolder.lyt_container = (LinearLayout)convertView.findViewById(R.id.lyt_container_ng);

            convertView.setTag(newsHolder);

        } else {

            newsHolder = (NewsHolder)convertView.getTag();
        }


        final NewsModel newsModel = _newsModels.get(position);

        newsHolder.lyt_container.getLayoutParams().width = Commons.screenwidth/3;
        int height = Commons.screenwidth/3;
        newsHolder.lyt_container.getLayoutParams().height = height*5/4;

        Glide.with(_activity).load(newsModel.get_news_imageurl()).placeholder(R.color.news_greay).into(newsHolder.imv_newsImage);
        newsHolder.txv_newsContent.setText(newsModel.get_news_content());

        newsHolder.imv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.showToast("News" + String.valueOf(position));
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.NewsDetailsFragment(newsModel);
            }
        });
        return convertView;
    }

    public class NewsHolder{

        ImageView imv_newsImage, imv_view;
        TextView txv_newsTitle,txv_newsDate, txv_newsContent;
        LinearLayout lyt_container;

    }
}
