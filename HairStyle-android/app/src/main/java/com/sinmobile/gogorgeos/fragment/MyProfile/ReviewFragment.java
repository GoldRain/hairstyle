package com.sinmobile.gogorgeos.fragment.MyProfile;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.RatingListAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.RatingModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ReviewFragment extends Fragment {

    Context _context;
    MainActivity _activity;
    View view;

    ListView lst_contain;
    RatingListAdapter _adapter;

    ArrayList<RatingModel> _ratings = new ArrayList<>();

    public ReviewFragment(Context context) {
        // Required empty public constructor

        _context = context;
        _activity = (MainActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_review, container, false);

        if (Commons.g_user != null){

        _ratings = Commons.g_user.get_user_ratings();
        }

        Log.d("=====ratings=========>", String.valueOf(_ratings));

        loadLayout();
        return view;
    }

    private void loadLayout() {

        lst_contain = (ListView)view.findViewById(R.id.lst_contain);
        _adapter = new RatingListAdapter(_activity, _ratings);
        lst_contain.setAdapter(_adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

}
