package com.sinmobile.gogorgeos.fragment.UserProfile;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.UserProfileViewPagerAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.customcalendar.fragment.CalendarViewPagerFragment;
import com.sinmobile.gogorgeos.fragment.MyAlertDialogFragment;
import com.sinmobile.gogorgeos.model.RatingModel;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.utils.RadiusImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDetailsFragment extends Fragment implements View.OnClickListener {

    MainActivity _activity;
    View view;
    UserModel _user = new UserModel();
    TabLayout tabLayout;
    ViewPager viewPager;
    UserProfileViewPagerAdapter _adapter_user;

    ArrayList<RatingModel> _ratings = new ArrayList<>();
    RatingModel ratingModel = new RatingModel();

    ImageView ui_imvBack, imv_calendar;
    RadiusImageView imv_photo;
    TextView txv_name, txv_location, txv_sever_location, txv_phoneNum, txv_email, txv_user_about;
    ImageView imv_facebook, imv_twitter, imv_youtube, imv_instagram, imv_snap;
    TextView txv_comment_review;
    LinearLayout lyt_mail, lyt_phone_call;

    SimpleRatingBar srb_review;
    EditText edt_review;
    TextView txv_submit, txv_cancel;

    FrameLayout frameLayout;

    int cur_rating = 0;

    private boolean isChoiceModelSingle = false;
    List<Long> _valuesArray = new ArrayList<>();

    public UserDetailsFragment(UserModel user) {
        // Required empty public constructor
        _user = user;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_details, container, false);

        _ratings.addAll(_user.get_user_ratings());

        if (_user.get_user_role() == 1) {

            Commons.ROLE_DETAILS = 1;

            _activity.setAppTitle(_activity.getString(R.string.artists_page));


        } else if (_user.get_user_role() == 2) {

            Commons.ROLE_DETAILS = 2;
            _activity.setAppTitle(_activity.getString(R.string.stylists_page));

        } else if (_user.get_user_role() == 3) {
            Commons.ROLE_DETAILS = 3;

            _activity.setAppTitle(_activity.getString(R.string.models_page));

        }
        loadLayout();
        // view_calendar();
        return view;
    }

    private void loadLayout() {

        ui_imvBack = (ImageView) _activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.onBackPressed();
            }
        });

        imv_calendar = (ImageView) _activity.findViewById(R.id.imv_calendar);
        imv_calendar.setOnClickListener(this);

        imv_photo = (RadiusImageView) view.findViewById(R.id.imv_photo);
        txv_name = (TextView) view.findViewById(R.id.txv_name);
        txv_location = (TextView) view.findViewById(R.id.txv_location);
        txv_sever_location = (TextView) view.findViewById(R.id.txv_sever_location);
        txv_phoneNum = (TextView) view.findViewById(R.id.txv_phone_num);
        txv_email = (TextView) view.findViewById(R.id.txv_email);
        txv_user_about = (TextView) view.findViewById(R.id.txv_user_about);

        lyt_mail = (LinearLayout) view.findViewById(R.id.lyt_mail);
        lyt_mail.setOnClickListener(this);

        lyt_phone_call = (LinearLayout) view.findViewById(R.id.lyt_phone_call);
        lyt_phone_call.setOnClickListener(this);

        imv_facebook = (ImageView) view.findViewById(R.id.imv_facebook);
        imv_facebook.setOnClickListener(this);

        imv_twitter = (ImageView) view.findViewById(R.id.imv_twitter);
        imv_twitter.setOnClickListener(this);

        imv_youtube = (ImageView) view.findViewById(R.id.imv_youTube);
        imv_youtube.setOnClickListener(this);

        imv_instagram = (ImageView) view.findViewById(R.id.imv_instagram);
        imv_instagram.setOnClickListener(this);

        imv_snap = (ImageView) view.findViewById(R.id.imv_snap);
        imv_snap.setOnClickListener(this);

        txv_comment_review = (TextView) view.findViewById(R.id.txv_comment_review);
        txv_comment_review.setOnClickListener(this);

        FragmentManager manager = _activity.getSupportFragmentManager();
        _adapter_user = new UserProfileViewPagerAdapter(manager, _activity);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setAdapter(_adapter_user);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupTabIcons();
        showUserData();

        // ================== All Data===============================
        //===================== My profile Api Call=====================================
    }

    public void showUserData() {

        Glide.with(_activity).load(_user.get_user_photoUrl()).placeholder(R.drawable.bg_non_profile).into(imv_photo);
        txv_name.setText(_user.get_user_fullname());
        txv_location.setText(_user.get_user_adress());
        txv_sever_location.setText(_user.get_serve_location());
        txv_phoneNum.setText(_user.get_user_phonenumber());
        txv_email.setText(_user.get_user_email());
        txv_user_about.setText(_user.get_user_aboutme());

        Commons.userprofile = _user;

    }

    public void setupTabIcons() {

        int[] tabIcons = {R.drawable.image_frag, R.drawable.video_frag, R.drawable.review_frag};

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imv_facebook:
                _activity.showToast("Facebook");
                break;

            case R.id.lyt_mail:

                if (Commons.g_user == null) {

                    _activity.showAlertDialog("You can't send mail. Please register login!");

                } else sendMail();
                break;

            case R.id.imv_twitter:
                break;

            case R.id.imv_youTube:
                break;

            case R.id.imv_instagram:
                _activity.showToast("Instagram");
                break;

            case R.id.imv_snap:
                break;

            case R.id.txv_comment_review:
                if (Commons.g_user == null) {

                    _activity.showAlertDialog("You can't give the review. Please register login!");

                } else wite_review();

                break;

            case R.id.imv_calendar:
                view_calendar();
                break;

            case R.id.lyt_phone_call:
                callPhone();
                break;
        }
    }

    private void sendMail() {

        _activity.showProgress();

        String to = "";
        String subject = "";
        String message = "";

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);

        //need this to prompts email client only
        email.setType("message/rfc822");

        startActivity(Intent.createChooser(email, "Choose an Email client :"));

        _activity.closeProgress();

    }

    private void view_calendar() {

        if (_user.get_user_available().size() != 0) {

            for (int i = 0; i < 12; i++) {

                long values = (long) _user.get_user_available().get(i).get_value();
                _valuesArray.add(values);

                Log.d("Value===>", String.valueOf(values));
            }

        } else Log.d("Null", "nulllllll");

        MyAlertDialogFragment dialogFragment = MyAlertDialogFragment.newInstance(isChoiceModelSingle, _valuesArray);

        dialogFragment.show(getFragmentManager().beginTransaction(), "DialogFragment");

    }

    private void callPhone() {

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData( Uri.parse("tel:" + txv_phoneNum.getText().toString().trim()));

        if (ActivityCompat.checkSelfPermission(_activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        _activity.startActivity(callIntent);
    }

    private void wite_review(){

        final Dialog dialog = new Dialog(_activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_write_review);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        edt_review = (EditText) dialog.findViewById(R.id.edt_review);

        txv_cancel = (TextView)dialog.findViewById(R.id.txv_cancel);
        txv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txv_submit = (TextView)dialog.findViewById(R.id.txv_submit);


        txv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.showProgress();

                String url = ReqConst.SERVER_URL + ReqConst.REQ_WRITEREVIEW;

                Log.d("Write--URL", url);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //parseWriteReview(response);
                        try {

                            JSONObject object = new JSONObject(response);
                            String result_message = object.getString(ReqConst.RES_MESSAGE);

                            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                                int id = object.getInt(ReqConst.RES_ID);

                                _activity.closeProgress();

                                ratingModel.set_rate_id(id);

                                ratingModel.set_rate_sender(Commons.g_user.get_user_id());
                                ratingModel.set_rate_receiver(_user.get_user_id());
                                ratingModel.set_rate_comment(edt_review.getText().toString());
                                ratingModel.set_rate_marks(cur_rating);
                                ratingModel.set_rate_sendername(Commons.g_user.get_user_fullname());
                                ratingModel.set_rate_senderphoto(Commons.g_user.get_user_photoUrl());

                                _ratings.add(ratingModel);
                              //  _adapter_user.notifyDataSetChanged();

                                Commons.userprofile.set_user_ratings(_ratings);
                                _activity.UserProfile(Commons.userprofile);

                                _activity.showToast("Successfully");

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        _activity.closeProgress();
                        _activity.showAlertDialog(_activity.getString(R.string.error));
                    }
                })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();

                        try {

                            params.put(ReqConst.PARAM_RATESENDER, String.valueOf(Commons.g_user.get_user_id()));
                            params.put(ReqConst.PARAM_RECEIVER, String.valueOf(_user.get_user_id()));
                            params.put(ReqConst.PARAM_RATEMARKS, String.valueOf(cur_rating));
                            params.put(ReqConst.PARAM_RATECOMMENT, edt_review.getText().toString() );

                        } catch (Exception e) {
                            _activity.closeProgress();

                            _activity.showAlertDialog(getString(R.string.error));
                        }
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);


                dialog.dismiss();
            }

        });

        srb_review = (SimpleRatingBar)dialog.findViewById(R.id.srb_review);
        srb_review.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                cur_rating = (int)rating;
                _activity.showToast(String.valueOf(cur_rating));
            }
        });

        dialog.show();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}
