package com.sinmobile.gogorgeos.fragment.MyProfile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.customcalendar.data.CalendarDate;
import com.sinmobile.gogorgeos.customcalendar.fragment.CalendarViewFragment;
import com.sinmobile.gogorgeos.customcalendar.fragment.CalendarViewPagerFragment;
import com.sinmobile.gogorgeos.model.AvailableModel;
import com.sinmobile.gogorgeos.model.RatingModel;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.model.VideoModel;
import com.sinmobile.gogorgeos.parses.RoleModelPaser;
import com.sinmobile.gogorgeos.preference.PrefConst;
import com.sinmobile.gogorgeos.preference.Preference;
import com.sinmobile.gogorgeos.utils.BitmapUtils;
import com.sinmobile.gogorgeos.utils.MonthDays;
import com.sinmobile.gogorgeos.utils.MultiPartRequest;
import com.sinmobile.gogorgeos.utils.PhotoSelectDialog;
import com.sinmobile.gogorgeos.utils.PlaceJSONParser;
import com.sinmobile.gogorgeos.utils.RadiusImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.sinmobile.gogorgeos.activity.MainActivity.MY_PEQUEST_CODE;
import static com.sinmobile.gogorgeos.commons.Commons.AVA_YEAR;
import static com.sinmobile.gogorgeos.commons.Commons.YEAR;


public class MyProfileEditFragment extends Fragment implements View.OnClickListener {


    static MyProfileEditFragment instance = null;

    UserModel _user = new UserModel();

    ArrayList<String> _user_images = new ArrayList<>();
    ArrayList<VideoModel> _user_videos = new ArrayList<>();
    ArrayList<RatingModel> _user_ratings = new ArrayList<>();
    ArrayList<AvailableModel> _user_available = new ArrayList<>();

    String _allYear = "";

    MainActivity _activity;
    ImageView ui_imvBack;
    View view ;
    RadiusImageView ui_imvMyPhoto;
    EditText edt_firstName, edt_lastName, edt_sever_location, edt_phoneNo, edt_emailID;
    EditText edt_facebook, edt_twitter, edt_youTube, edt_instagram, edt_Snap;

    String _firstNam1 = "", _lastName1 = "", _address1 = "", _phoneNumber1 = "",_sever_location1 = "", _user_email1 = "", _facebook1 = "", _twitter1 = "", _youtube1 = "", _instagram1 = "", _snap1="",_aboutme1 = "";
    String _firstNam = "", _lastName = "", _address = "",_sever_location = "", _phoneNumber = "", _user_email = "", _facebook = "", _twitter = "", _youtube = "", _instagram = "", _snap = "",_aboutme = "";

    String year = "";
    String _JSON_VALUE = "";

    //CalendarView calendar;

    EditText edt_aboutme;
    public static TextView btn_submit;
    String _photoPath = "";
    private Uri _imageCaptureUri;
    private PhotoSelectDialog _photoSelectDialog;


    public TextView tv_date;
    private List<CalendarDate> mListDate = new ArrayList<>();

    private boolean isChoiceModelSingle = false;
    List<Long> _valuesArray = new ArrayList<>();
    MonthDays init = new MonthDays();

    //================= Autocomplete=======================
    AutoCompleteTextView atvfrompostalstreet;
    PlacesTask placesTask;
    ParserTask parserTask;


    /*////////////////////////////////////////////////////////////////////////*********START*******///////////////////////////////////////////////////////////////////////////////////////////////*/

    public MyProfileEditFragment() {
        // Required empty public constructor
        instance = this;
    }

    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.RECORD_AUDIO, Manifest.permission.BIND_VOICE_INTERACTION};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =  inflater.inflate(R.layout.fragment_my_profile_edit, container, false);
        tv_date = (TextView)view.findViewById(R.id.tv_date);
        tv_date.setText(Calendar.getInstance().get(Calendar.YEAR) + "-" + (Calendar.getInstance().get(Calendar.MONTH)+1));

        Commons.PROFILE_EDIT = 1;
        Commons.MY_PROFILE = 1;
        Commons.valuesArray = new ArrayList<>();

        loadLayout();

        initFragment();

        Log.d("Common_array==>:", String.valueOf(Commons.g_user.get_user_available()));

        return view;
    }

    /*///////////////////////////////////CUSTOM CALENDAR START////////////////////////////////////////////////////////*/

    private void initFragment(){

        if (Commons.g_user.get_user_available().size() == 0){

            init.initAvailableYear(YEAR);

            _valuesArray = (Commons.AVAILABLE_YEAR);

        } else {

            for (int i = 0; i < 12; i++){

                long value =  Commons.g_user.get_user_available().get(i).get_value();

                _valuesArray.add(value);
            }
        }

        FragmentManager fm = _activity.getSupportFragmentManager();
        FragmentTransaction tx = fm.beginTransaction();
        // Fragment fragment = new CalendarViewPagerFragment();
        Fragment fragment = CalendarViewPagerFragment.newInstance(isChoiceModelSingle, _valuesArray );
        tx.replace(R.id.fl_content, fragment);
        tx.commit();

    }

/*//////////////////////////////////END////////////////////////////////////////////////*/


    private void loadLayout() {

        atvfrompostalstreet = (AutoCompleteTextView) view.findViewById(R.id.edt_location);
        atvfrompostalstreet.setThreshold(1);
        atvfrompostalstreet.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        ui_imvBack = (ImageView)_activity.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvMyPhoto = (RadiusImageView)view.findViewById(R.id.imv_photo);
        ui_imvMyPhoto.setOnClickListener(this);

        edt_firstName = (EditText)view.findViewById(R.id.edt_firstName);
        edt_lastName = (EditText)view.findViewById(R.id.edt_lastName);
        edt_sever_location = (EditText)view.findViewById(R.id.edt_sever_location);
       // edt_location = (EditText)view.findViewById(R.id.edt_location);
        edt_phoneNo = (EditText)view.findViewById(R.id.edt_phone_no);
        edt_emailID = (EditText)view.findViewById(R.id.edt_emailId);

        edt_facebook = (EditText)view.findViewById(R.id.edt_facebook);
        edt_twitter = (EditText)view.findViewById(R.id.edt_twitter);
        edt_youTube = (EditText)view.findViewById(R.id.edt_youtobe);
        edt_instagram = (EditText)view.findViewById(R.id.edt_instagram);
        edt_Snap = (EditText)view.findViewById(R.id.edt_snap);

        edt_aboutme = (EditText)view.findViewById(R.id.edt_content);

        btn_submit = (TextView)view.findViewById(R.id.txv_submit);
        btn_submit.setOnClickListener(this) ;

       // calendar = (CalendarView)view.findViewById(R.id.calendar);
        //calendar.setOnDateChangeListener(this);

        if (Commons.g_user != null){
        showProfile();
        }

    }

    private void showProfile(){

        UserModel myProfile = Commons.g_user;

        Glide.with(_activity).load(myProfile.get_user_photoUrl()).placeholder(R.drawable.bg_non_profile).into(ui_imvMyPhoto);
        edt_firstName.setText(myProfile.get_user_firstname());
        edt_lastName.setText(myProfile.get_user_lastname());
        atvfrompostalstreet.setText(myProfile.get_user_adress());
        edt_sever_location.setText(myProfile.get_serve_location());
        edt_phoneNo.setText(myProfile.get_user_phonenumber());
        edt_emailID.setText(myProfile.get_user_email());
        edt_facebook.setText(myProfile.get_user_fblink());
        edt_twitter.setText(myProfile.get_user_twlink());
        edt_youTube.setText(myProfile.get_user_yolink());
        edt_instagram.setText(myProfile.get_user_inlink());
        edt_Snap.setText(myProfile.get_user_snlink());
        edt_aboutme.setText(myProfile.get_user_aboutme());

    }

    public void setMyPhoto(){

        View.OnClickListener cameraListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakePhotoAction();
                if (!_activity.hasPermissions(_activity, PERMISSIONS)){

                    ActivityCompat.requestPermissions(_activity, PERMISSIONS, MY_PEQUEST_CODE);
                }else {
                }
                _photoSelectDialog.dismiss();
            }
        };
        View.OnClickListener albumListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakeGallery();
                _photoSelectDialog.dismiss();
            }
        };

        View.OnClickListener cancelListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _photoSelectDialog.dismiss();
            }
        };

            _photoSelectDialog = new PhotoSelectDialog(_activity, cameraListener, albumListener, cancelListener);

        _photoSelectDialog.show();
    }

    public void doTakePhotoAction(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        if (_imageCaptureUri != null){
            intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        }

        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    public void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void onActivityResult(int requestCOde, int resultCode, Intent data){

        switch (requestCOde){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(_activity);

                        InputStream in = _activity.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);

                        in.close();

                        //set The bitmap data to image View
                        ui_imvMyPhoto.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                        ///uploadImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {
                    _photoPath = BitmapUtils.getRealPathFromURI(_activity, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(_activity)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            }
        }
    }

    private void selectUpdate(){

        if (_photoPath.length() != 0){
            updateProfile();

        } else updateProfile_String();

    }

    private void updateProfile(){


       // File file;
         File file = new File(_photoPath);

        if (edt_firstName.getText().toString().length() != 0){

            _firstNam = edt_firstName.getText().toString();

        }
        if ( edt_lastName.getText().toString().length() != 0){

            _lastName = edt_lastName.getText().toString();

        }
        if (atvfrompostalstreet.getText().toString().length() != 0){

            _address = atvfrompostalstreet.getText().toString();

        } if (edt_sever_location.getText().toString().length() != 0){

            _sever_location = edt_sever_location.getText().toString();
        }
        if (edt_phoneNo.getText().toString().length() != 0){

            _phoneNumber = edt_phoneNo.getText().toString();
        }
        if (edt_emailID.getText().toString().length() != 0){
            _user_email = edt_emailID.getText().toString();

        } else if (edt_facebook.getText().toString().length() != 0){

            _facebook = edt_facebook.getText().toString();
        }
        if (edt_twitter.getText().toString().length() != 0){

            _twitter = edt_twitter.getText().toString();

        }
        if (edt_youTube.getText().toString().length() != 0){
            _youtube = edt_youTube.getText().toString();

        }
        if (edt_instagram.getText().toString().length() != 0) {
            _instagram = edt_instagram.getText().toString();

        }
        if (edt_Snap.getText().toString().length() != 0){
            _snap = edt_Snap.getText().toString();

        }
        if (edt_aboutme.getText().toString().length() != 0){
            _aboutme = edt_aboutme.getText().toString();
        }

        _activity.showProgress();

        try {

            Map<String, String> params = new HashMap<>();

            params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_user_id()));
            params.put(ReqConst.PARAM_USER_FIRSTNAME, _firstNam);
            params.put(ReqConst.PARAM_USER_LASTNAME, _lastName);
            params.put(ReqConst.PARAM_USER_SEVERLOCATION, _sever_location);
            params.put(ReqConst.PARAM_USER_PHONENUMBER, _phoneNumber);
            params.put(ReqConst.PARAM_USEREMAIL, _user_email);
            params.put(ReqConst.PARAM_USER_ADDRESS, _address);
            params.put(ReqConst.PARAM_FBLINK, _facebook);
            params.put(ReqConst.PARAM_TWLINK, _twitter);
            params.put(ReqConst.PARAM_YOLINK, _youtube);
            params.put(ReqConst.PARAM_INLINK, _instagram);
            params.put(ReqConst.PARAM_SNLINK, _snap);
            params.put(ReqConst.PARAM_ABOUTME, _aboutme);

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATEPROFILE;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(_activity.getString(R.string.error));


                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    parseUpdateProfile(json);

                }

            }, file, ReqConst.REQ_PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(60000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            GogorgeosApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void updateProfile_String(){

        if (edt_firstName.getText().toString().length() != 0){

            _firstNam1 = edt_firstName.getText().toString();

        }
        if ( edt_lastName.getText().toString().length() != 0){

            _lastName1 = edt_lastName.getText().toString();

        }
        if (atvfrompostalstreet.getText().toString().length() != 0){

            _address1 = atvfrompostalstreet.getText().toString();
        }
        if (edt_sever_location.getText().toString().length() != 0 ){

            _sever_location1 = edt_sever_location.getText().toString();
        }
        if (edt_phoneNo.getText().toString().length() != 0){

            _phoneNumber1 = edt_phoneNo.getText().toString();

        }
        if (edt_emailID.getText().toString().length() != 0){
            _user_email1 = edt_emailID.getText().toString();
        }
        if (edt_facebook.getText().toString().length() != 0){

            _facebook1 = edt_facebook.getText().toString();
        }
        if (edt_twitter.getText().toString().length() != 0){

            _twitter1 = edt_twitter.getText().toString();
        }
        if (edt_youTube.getText().toString().length() != 0){
            _youtube1 = edt_youTube.getText().toString();
        }
        if (edt_instagram.getText().toString().length() != 0) {
            _instagram1 = edt_instagram.getText().toString();
        }
        if (edt_Snap.getText().toString().length() != 0){
            _snap1= edt_Snap.getText().toString();
        }
        if (edt_aboutme.getText().toString().length() != 0){
            _aboutme1 = edt_aboutme.getText().toString();
        }


        String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATEPROFILE;

        _activity.showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdateProfile(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_user_id()));
                    params.put(ReqConst.PARAM_USER_FIRSTNAME, _firstNam1);
                    params.put(ReqConst.PARAM_USER_LASTNAME, _lastName1);
                    params.put(ReqConst.PARAM_USER_PHONENUMBER, _phoneNumber1);
                    params.put(ReqConst.PARAM_USEREMAIL, _user_email1);
                    params.put(ReqConst.PARAM_USER_ADDRESS, _address1);
                    params.put(ReqConst.PARAM_USER_SEVERLOCATION, _sever_location1);
                    params.put(ReqConst.PARAM_FBLINK, _facebook1);
                    params.put(ReqConst.PARAM_TWLINK, _twitter1);
                    params.put(ReqConst.PARAM_YOLINK, _youtube1);
                    params.put(ReqConst.PARAM_INLINK, _instagram1);
                    params.put(ReqConst.PARAM_SNLINK, _snap1);
                    params.put(ReqConst.PARAM_ABOUTME, _aboutme1);

                    if (_photoPath.length() == 0 && Commons.g_user.get_user_photoUrl().length() == 0) {

                        params.put(ReqConst.PARAM_USERPHOTO, "");
                    } else {
                    params.put(ReqConst.PARAM_USERPHOTO, Commons.g_user.get_user_photoUrl());
                    }

                } catch (Exception e) {

                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void parseUpdateProfile(String response){

        Log.d("Update_profile==>", response);

        try {

            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);

            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                JSONObject user = object.getJSONObject(ReqConst.RES_USER);

                _user = new UserModel();

                _user.set_user_id(user.getInt(ReqConst.RES_USERID));
                _user.set_user_firstname(user.getString(ReqConst.PARAM_USER_FIRSTNAME));
                _user.set_user_lastname(user.getString(ReqConst.PARAM_USER_LASTNAME));
                _user.set_user_email(user.getString(ReqConst.PARAM_USEREMAIL));
                _user.set_user_gender(user.getInt(ReqConst.PARAM_USERGENDER));
                _user.set_user_birthday(user.getString(ReqConst.PARAM_USER_BIRTHDAY));
                _user.set_user_subscribed(user.getInt(ReqConst.PARAM_USER_SUBSCRIBED));
                _user.set_user_role(user.getInt(ReqConst.PARAM_USERROLE));
                _user.set_user_phonenumber(user.getString(ReqConst.PARAM_USER_PHONENUMBER));
                _user.set_user_adress(user.getString(ReqConst.PARAM_USER_ADDRESS));
                _user.set_serve_location(user.getString(ReqConst.PARAM_USER_SEVERLOCATION));
                _user.set_user_photoUrl(user.getString(ReqConst.PARAM_USER_PHOTOURL));
                _user.set_user_fblink(user.getString(ReqConst.PARAM_FBLINK));
                _user.set_user_twlink(user.getString(ReqConst.PARAM_TWLINK));
                _user.set_user_yolink(user.getString(ReqConst.PARAM_YOLINK));
                _user.set_user_inlink(user.getString(ReqConst.PARAM_INLINK));
                _user.set_user_snlink(user.getString(ReqConst.PARAM_SNLINK));
                _user.set_user_aboutme(user.getString(ReqConst.PARAM_ABOUTME));


                /*USER_IMAGES*/
                JSONArray user_images = user.getJSONArray(ReqConst.RES_USERIMAGES);

                for (int i = 0; i < user_images.length(); i++){

                    JSONObject jsonImage = (JSONObject) user_images.get(i);
                    _user_images.add(jsonImage.getString(ReqConst.PARAM_USER_IMAGEURL));

                }
                _user.set_user_images(_user_images);

                /*USER_VIDEOS*/
                JSONArray user_videos = user.getJSONArray(ReqConst.RES_USERVIDEOS);
                for (int i = 0; i < user_videos.length(); i++){

                    JSONObject jsonVideos = (JSONObject)user_videos.get(i);

                    VideoModel video = new VideoModel();

                    video.set_video_id(jsonVideos.getInt(ReqConst.RES_VIDEOID));
                    video.set_video_url(jsonVideos.getString(ReqConst.RES_VIDEO_URL));
                    video.set_video_thumbimageurl(jsonVideos.getString(ReqConst.RES_THUMB_IMAGEURL));
                    video.set_video_comment(jsonVideos.getString(ReqConst.RES_VIDEO_COMMENT));

                    _user_videos.add(video);
                }
                _user.set_user_video(_user_videos);

                /*USER_RATINGS*/
                JSONArray user_ratings = user.getJSONArray(ReqConst.RES_USERRATINGS);
                for (int i = 0; i < user_ratings.length(); i++ ){

                    JSONObject jsonRatings = (JSONObject) user_ratings.get(i);
                    RatingModel _rating = new RatingModel();

                    _rating.set_rate_sender(jsonRatings.getInt(ReqConst.RES_RATESENDER));
                    _rating.set_rate_senderphoto(jsonRatings.getString(ReqConst.RES_RATESENDER_PHOTOURL));
                    _rating.set_rate_sendername(jsonRatings.getString(ReqConst.RES_RATESENDERNAME));
                    _rating.set_rate_receiver(jsonRatings.getInt(ReqConst.RES_RATERECEIVER));
                    _rating.set_rate_marks(jsonRatings.getInt(ReqConst.RES_RATEMARKS));
                    _rating.set_rate_comment(jsonRatings.getString(ReqConst.RES_RATEREVIEW));

                    _user_ratings.add(_rating);
                }

                _user.set_user_ratings(_user_ratings);


                JSONArray user_available = user.getJSONArray(ReqConst.RES_USER_AVAILABLE);

                for (int i = 0; i < user_available.length(); i++){

                    JSONObject jsonAvailable = (JSONObject)user_available.get(i);

                    AvailableModel available = new AvailableModel();

                    available.set_year(jsonAvailable.getInt(ReqConst.RARAM_YEAR));
                    available.set_month(jsonAvailable.getInt(ReqConst.RARAM_MONTH));
                    available.set_value(jsonAvailable.getLong(ReqConst.RARAM_VALUE));

                    _user_available.add(available);
                }

                _user.set_user_available(_user_available);

                Preference.getInstance().put(_activity, PrefConst.PREFKEY_USERPWD, Commons.g_user.get_user_password());

                Preference.getInstance().put(_activity, PrefConst.PREFKEY_USEREMAIL, edt_emailID.getText().toString());

                String pwd = Commons.g_user.get_user_password();
                Commons.g_user = _user;
                Commons.g_user.set_user_password(pwd);

                if(Commons.valuesArray.size() == 0){

                    _activity.closeProgress();

                    Log.d("PWD==", Commons.g_user.get_user_password());
                    Log.d("Email==>", edt_emailID.getText().toString());
                    Log.d("====JSON_VALUE==>", _JSON_VALUE);

                    _activity.showToast("Success update");

                    _activity.MyProfileEditFragment();

                } else setAvailable();
            }

        } catch (JSONException e) {
            _activity.closeProgress();
            _activity.showAlertDialog(_activity.getString(R.string.error));
            e.printStackTrace();
        }
    }

    public void allAvailable(ArrayList<Long> values){

        _allYear = new String();

        for (int k = 0; k < 12; k++){

            Log.d("=====value==>", String.valueOf(values.get(k)));

            if (k == 11){

                year = (AVA_YEAR + "-" + (k+1) +"-" + values.get(k) );

            } else year = (AVA_YEAR + "-" + (k+1) +"-" + values.get(k) + ":");

            _allYear += year;
        }

        _JSON_VALUE = _allYear;

        Log.d("_JSON_VALUE==>", String.valueOf(_JSON_VALUE));

    }

    private void setAvailable(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SETAVAILABLE;
        StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseAvailable(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    Log.d("commonsvalue====", String.valueOf(Commons.valuesArray));

                    for (int k = 0; k < 12; k++){

                        Log.d("=====value==>", String.valueOf(Commons.valuesArray.get(k)));

                        if (k == 11){

                            year = (AVA_YEAR + "-" + (k+1) +"-" + Commons.valuesArray.get(k) );

                        } else year = (AVA_YEAR + "-" + (k+1) +"-" + Commons.valuesArray.get(k) + ":");

                        _allYear += year;
                    }

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_user_id()));
                    params.put(ReqConst.PARAM_AVAILABLE, _allYear);

                } catch (Exception e) {

                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);

    }
    private void parseAvailable(String response){

        try {
            _activity.closeProgress();

            Log.d("parse_available==>", response);

            JSONObject object = new JSONObject(response);

            String result_message= object.getString(ReqConst.RES_MESSAGE);

            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                _activity.showToast(result_message);

                _user_available.clear();

                for (int j = 0; j < _valuesArray.size();j++){

                    AvailableModel availableModel = new AvailableModel();

                    availableModel.set_month(j+1);
                    availableModel.set_year(YEAR);
                    availableModel.set_value(_valuesArray.get(j));

                    _user_available.add(availableModel);
                }

                Commons.g_user.set_user_available(_user_available);

                _activity.MyProfileEditFragment();

            } else _activity.showAlertDialog(result_message);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                Commons.MY_PROFILE = 0;
                _activity.onBackPressed();
                break;

            case R.id.imv_photo:

                setMyPhoto();
                break;

            case R.id.txv_submit:
                selectUpdate();
                //setAvailable();
                break;

        }
    }


  //======================= AutoComplete========================
   private class PlacesTask extends AsyncTask<String, Void, String> {

       @Override
       protected String doInBackground(String... place) {
           // For storing data from web service
           String data = "";

           // Obtain browser key from https://code.google.com/apis/console
           String key = "key=AIzaSyB_R9NUFK4G5lf-Zpr1wUmXDQ0WWZecYzA";

           String input = "";

           try {
               input = "input=" + URLEncoder.encode(place[0], "utf-8");
           } catch (UnsupportedEncodingException e1) {
               e1.printStackTrace();
           }

           // place type to be searched
           String types = "types=geocode";

           // Sensor enabled
           String sensor = "sensor=false";

           // Building the parameters to the web service
           String parameters = input + "&" + types + "&" + sensor + "&" + key;

           // Output format
           String output = "json";

           // Building the url to the web service
           String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;

           try {
               // Fetching the data from we service
               data = downloadUrl(url);
           } catch (Exception e) {
               Log.d("Background Task", e.toString());
           }
           return data;
       }

       @Override
       protected void onPostExecute(String result) {
           super.onPostExecute(result);

           // Creating ParserTask
           parserTask = new ParserTask();

           // Starting Parsing the JSON string returned by Web Service
           parserTask.execute(result);
       }
   }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};

            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(_activity.getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

            // Setting the adapter
            atvfrompostalstreet.setAdapter(adapter);

        }
    }

    /**
     * A method to download json data from url
     */
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();


            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


}
