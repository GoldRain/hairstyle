package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.preference.PrefConst;
import com.sinmobile.gogorgeos.preference.Preference;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by victory on 1/16/2017.
 */

public class SideMenuAdapter extends BaseAdapter {

    Context _context;

    ArrayList<String> menuItems = new ArrayList<>();

    public SideMenuAdapter(Context context, ArrayList<String> menuItems) {
        _context = context;
        this.menuItems = menuItems;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return menuItems.size();

    }

    @Override
    public Object getItem(int position) {
        return menuItems.get(position);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        int viewType = this.getItemViewType(position);

        if (viewType == 0) {

            SignInUp holder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.drawer_header,null);

                holder = new SignInUp(convertView);

                convertView.setTag(holder);
            } else {
                holder = (SignInUp)convertView.getTag();
            }

        } else {

            MenuItemHolder holder;
            if (convertView == null) {

                LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.layout_menu_item, null);

                holder = new MenuItemHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (MenuItemHolder) convertView.getTag();
            }

            holder.ui_txvItem.setText((String)getItem(position));

            if (Preference.getInstance().getValue(_context, PrefConst.PREFKEY_CURRNETLANG, 0) == 1){
                holder.ui_txvItem.setGravity(Gravity.RIGHT);
            }

        }

        return convertView;
    }

    class SignInUp {

        @Bind(R.id.txv_signIn) TextView ui_txvName;
        @Bind(R.id.txv_signUp) TextView ui_txvEmail;
        @Bind(R.id.lyt_header)LinearLayout ui_lytHeader;

        public SignInUp(View view) {
            ButterKnife.bind(this, view);
        }
    }

    class MenuItemHolder {

        @Bind(R.id.txv_item) TextView ui_txvItem;

        public MenuItemHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
