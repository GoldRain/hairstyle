package com.sinmobile.gogorgeos.fragment.Training;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.LocationMapsActivity;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.model.TrainingModel;


public class TrainingDetailsFragment extends Fragment {

    MainActivity _activity;
    View view;

    ImageView imv_back;

    TrainingModel _train = new TrainingModel();

    TextView txv_trainTitle, txv_trainLocation, txv_trainPhone, txv_trainMail, txv_trainPrice, txv_trainContent;
    ImageView imv_trainImage;

    LinearLayout lyt_location_train, lyt_phone_call, lyt_send_mail;

    public TrainingDetailsFragment(TrainingModel trainingModel) {
        // Required empty public constructor

        _train = trainingModel;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_training_details, container, false);

        Commons.ROLE_DETAILS = 4;
        loadLayout();
        return view;
    }

    private void loadLayout() {
        imv_back = (ImageView) _activity.findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.onBackPressed();
            }
        });

        lyt_location_train = (LinearLayout) view.findViewById(R.id.lyt_location_train);
        lyt_location_train.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(_activity, LocationMapsActivity.class);
                intent.putExtra(Constants.KEY_LOCATION, _train.get_training_location());
                _activity.startActivity(intent);

                _activity.overridePendingTransition(0, 0);

            }
        });

        lyt_phone_call = (LinearLayout) view.findViewById(R.id.lyt_phone_call);
        lyt_phone_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData( Uri.parse("tel:" + txv_trainPhone.getText().toString().trim()));
                if (ActivityCompat.checkSelfPermission(_activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                _activity.startActivity(callIntent);

            }
        });

        lyt_send_mail = (LinearLayout)view.findViewById(R.id.lyt_send_mail);
        lyt_send_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMail();
            }
        });

        txv_trainTitle = (TextView)view.findViewById(R.id.txv_trainTitle);
        txv_trainLocation = (TextView)view.findViewById(R.id.txv_location);
        txv_trainPhone = (TextView)view.findViewById(R.id.txv_phone_num);
        txv_trainMail = (TextView)view.findViewById(R.id.txv_email);
        txv_trainPrice = (TextView)view.findViewById(R.id.txv_trainPrice);
        txv_trainContent = (TextView)view.findViewById(R.id.txv_trainContent);
        imv_trainImage = (ImageView)view.findViewById(R.id.imv_trainImage);

        showTrainDetail();

    }

    private void showTrainDetail(){

        Log.d("===title", _train.get_training_title());
        Log.d("====phone", _train.get_training_phonenumber());

        txv_trainTitle.setText(_train.get_training_title());
        txv_trainLocation.setText(_train.get_training_location());
        txv_trainPhone.setText(_train.get_training_phonenumber());
        txv_trainMail.setText(_train.get_training_email());
        txv_trainPrice.setText("$"+_train.get_training_price());
        txv_trainContent.setText(_train.get_training_content());

        Glide.with(_activity).load(_train.get_training_imageurl()).placeholder(R.color.news_greay).into(imv_trainImage);

    }

    private void sendMail(){

        _activity.showProgress();

        String to ="";
        String subject ="" ;
        String message ="";

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);

        //need this to prompts email client only
        email.setType("message/rfc822");

        startActivity(Intent.createChooser(email, "Choose an Email client :"));

        _activity.closeProgress();

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}
