package com.sinmobile.gogorgeos.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VerificationActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    EditText ui_edtOtp;
    TextView ui_txvSubmit;

    ForgetActivity f;

    UserModel userModel = new UserModel();
    int _id = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        Commons.g_user = new UserModel();
        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView) findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_edtOtp = (EditText)findViewById(R.id.edt_otp);

        ui_txvSubmit = (TextView)findViewById(R.id.txv_submit);
        ui_txvSubmit.setOnClickListener(this);

        // container
        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.activity_verification);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtOtp.getWindowToken(), 0);
                return false;
            }
        });
    }

    private boolean checkValue(){

        if (ui_edtOtp.getText().length() == 0){

            showAlertDialog(getString(R.string.input_otp));
            return false;
        }
        return true;
    }



    private void progressRegister() {

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_REGISTER_USER;

        Log.d("URL=========", url);

        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                parseSignupResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        closeProgress();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_USER_FIRSTNAME, Commons.REGISTER_USER.get_user_lastname());
                    params.put(ReqConst.PARAM_USER_LASTNAME, Commons.REGISTER_USER.get_user_lastname());
                    params.put(ReqConst.PARAM_USEREMAIL, Commons.REGISTER_USER.get_user_email());
                    params.put(ReqConst.PARAM_USERPWD, Commons.REGISTER_USER.get_user_password());
                    params.put(ReqConst.PARAM_USERGENDER, String.valueOf(Commons.REGISTER_USER.get_user_gender()));
                    params.put(ReqConst.PARAM_USER_BIRTHDAY, Commons.REGISTER_USER.get_user_birthday());
                    params.put(ReqConst.PARAM_USER_SUBSCRIBED, String.valueOf(Commons.REGISTER_USER.get_user_subscribed()));
                    params.put(ReqConst.PARAM_USERROLE, String.valueOf(Commons.REGISTER_USER.get_user_role()));
                    params.put(ReqConst.PARAM_USER_VERICODE, ui_edtOtp.getText().toString());

                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseSignupResponse(String response) {

        Log.d("respons++register==", response);

        try {

            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);


            if (result_message.equals(ReqConst.CODE_SUCCESS)) {

                _id = object.getInt(ReqConst.RES_USERID);

                closeProgress();

                Commons.g_user = Commons.REGISTER_USER;
                Commons.g_user.set_user_id(_id);
                progressMain();

            }else {

                showAlertDialog(result_message);
                closeProgress();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            showAlertDialog(getString(R.string.error));
            closeProgress();
        }

    }



    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                onBackPressed();
                break;

            case R.id.txv_submit:

                if (checkValue()) {

                    if (f._forget == true) {
                        f._forget = false;
                        gotoNewPWD();

                    } else {

                        progressRegister();
                    }
                }
                break;
        }
    }

    private void gotoRegister(){

        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoNewPWD(){

        Intent intent = new Intent(this, NewPWDActivity.class);
        startActivity(intent);
        finish();
    }

    private void progressMain(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

        if (f._forget == true){

           Intent intent  = new Intent(this, ForgetActivity.class);
            overridePendingTransition(0,0);
            startActivity(intent);
            finish();

        }else gotoRegister();
    }
}
