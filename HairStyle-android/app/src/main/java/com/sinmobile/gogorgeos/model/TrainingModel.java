package com.sinmobile.gogorgeos.model;

import java.io.Serializable;

/**
 * Created by ToSuccess on 1/22/2017.
 */

public class TrainingModel implements Serializable {

    int _training_id = 0;
    String _training_title = "";
    String _training_date = "";
    String _training_location = "";
    String _training_email = "";
    String _training_imageurl = "";
    String _training_content = "";
    double _training_price = 0.0;
    String _training_phonenumber = "";


    public int get_training_id() {
        return _training_id;
    }

    public void set_training_id(int _training_id) {
        this._training_id = _training_id;
    }

    public String get_training_title() {
        return _training_title;
    }

    public void set_training_title(String _training_title) {
        this._training_title = _training_title;
    }

    public String get_training_date() {
        return _training_date;
    }

    public void set_training_date(String _training_date) {
        this._training_date = _training_date;
    }

    public String get_training_location() {
        return _training_location;
    }

    public void set_training_location(String _training_location) {
        this._training_location = _training_location;
    }

    public String get_training_email() {
        return _training_email;
    }

    public void set_training_email(String _training_email) {
        this._training_email = _training_email;
    }

    public String get_training_imageurl() {
        return _training_imageurl;
    }

    public void set_training_imageurl(String _training_imageurl) {
        this._training_imageurl = _training_imageurl;
    }

    public String get_training_content() {
        return _training_content;
    }

    public void set_training_content(String _training_content) {
        this._training_content = _training_content;
    }

    public double get_training_price() {
        return _training_price;
    }

    public void set_training_price(double _training_price) {
        this._training_price = _training_price;
    }

    public String get_training_phonenumber() {
        return _training_phonenumber;
    }

    public void set_training_phonenumber(String _training_phonenumber) {
        this._training_phonenumber = _training_phonenumber;
    }
}
