package com.sinmobile.gogorgeos.fragment.UserProfile;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.VideoGridViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.model.VideoModel;

import java.util.ArrayList;


public class UserVideoFragment extends Fragment {

    MainActivity _activity;
    View view;
    ArrayList<VideoModel> _videos = new ArrayList<>();
    GridView gdv_video;
    VideoGridViewAdapter _adapter;

    public UserVideoFragment(Context _context) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_user_video, container, false);

        _videos = Commons.userprofile.get_user_video();
        loadLayout();
        return view;
    }

    private void loadLayout() {


        gdv_video = (GridView)view.findViewById(R.id.gdv_user_video);
        _adapter = new VideoGridViewAdapter(_activity);
        gdv_video.setAdapter(_adapter);

        _adapter.setVideo(_videos);
        _adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

}

