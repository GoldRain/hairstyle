package com.sinmobile.gogorgeos.fragment.UserProfile;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.ImageGridViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.customview.SquareImageView;
import com.sinmobile.gogorgeos.utils.BitmapUtils;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


public class UserImageFragment extends Fragment {

    Context _context;
    MainActivity _activity;
    GridView gdv_image;
    ImageGridViewAdapter _adapter;

    ArrayList<String> _images = new ArrayList<>();
    View view;

    Uri _imageCaptureUri;
    String _photoPath;
    SquareImageView imv_image;

    public UserImageFragment(Context context) {
        // Required empty public constructor
        _context = context;
        _activity = (MainActivity)getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_user_image, container, false);

        _images = Commons.userprofile.get_user_images();

        loadLayout();
        return view;
    }

    private void loadLayout() {

        Log.d("USERID", String.valueOf(Commons.userprofile.get_user_id()));

        Log.d("========images==?", String.valueOf(_images));
        imv_image = (SquareImageView)view.findViewById(R.id.imv_image);

        gdv_image = (GridView)view.findViewById(R.id.grd_user_image);
        _adapter = new ImageGridViewAdapter(_activity);
        gdv_image.setAdapter(_adapter);

        _adapter.setImages(_images);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

/*    @Override
    public void onResume() {

        _adapter.setImages(_images);
        _adapter.notifyDataSetChanged();
        super.onResume();
    }*/
}
