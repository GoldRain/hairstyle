package com.sinmobile.gogorgeos.commons;

import android.content.Context;
import android.os.Handler;
import android.util.TypedValue;

import com.sinmobile.gogorgeos.customcalendar.data.CalendarDate;
import com.sinmobile.gogorgeos.model.FaqModel;
import com.sinmobile.gogorgeos.model.UserModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class Commons {

    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;

    public static int SCREEN_WIDTH = 0;

    public static int SCREEN_HEIGHT = 0;

    public static int STATUSBAR_HEIGHT = 0;

    public static int SHARE_OPEN_COUNT = 3;

    public static Handler g_handler = null;

    public static String g_deviceId = null;


    public static String g_appPath = "";

    public static String g_appVersion = "1.0";

    public static UserModel myprofile = null;
    public static UserModel userprofile = null;

    public static UserModel g_newUser = null;
    public static UserModel g_user = null;

    public static UserModel REGISTER_USER = null;

    public static int PROFILE_EDIT = 0;
    public static int ROLE_DETAILS = 0;  /*ROLE_DETAILS //1: ARTIST, 2:STYLIST, 3:MODELS, 4:NEWS, 5:TRAINING*/
    public static int ROLE_STATE = 0;

    public static int screenwidth = 0;
    public static int screenheight = 0;
    public static int SEARCH_PARAM = 0;  /*1:name, 2:date, 3:location*/
    public static int SEARCH_ROLE = 1;   /*1:artist, 2:style, 3:model*/

    public static String SEARCH_EDIT = "";
    public static ArrayList<Integer> selectedDate = new ArrayList<>();

    public static ArrayList<FaqModel> FAQS = null;
    public static String ABOUTUS_TITLE = "";
    public static String ABOUTUS_CONTENT = "";
    public static String EMAIL = "";
    public static String PHONENUMBER = "";
    public static ArrayList<String> ADDRESS = null;
    public static int YEAR = 0;
    public static String MONTH = "";
    public static String VALUE = "";

    public static Integer AVA_YEAR = 0;
    public static ArrayList<Integer> AVA_MONTH = null;
    public static ArrayList<Integer> AVA_DAY = null;

    public static ArrayList<Long> AVAILABLE_YEAR = null;

    public static List<Long> valuesArray = new ArrayList<>();
    public static int MY_PROFILE = 0;

    public static int LANGUAGE = 0;

    //=================================


    public static int GetPixelValueFromDp(Context context, float dp_value) {

        int pxValue = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp_value, context.getResources()
                        .getDisplayMetrics());

        return pxValue;
    }

   /* public static String idxToAddr(int idx) {
        return idx + "@" + ReqConst.CHATTING_SERVER;
    }
*/
    public static int addrToIdx(String addr) {
        int pos = addr.indexOf("@");
        return Integer.valueOf(addr.substring(0, pos)).intValue();
    }

    public static String fileExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf(".") == -1) {
            return url;
        } else {
            String ext = url.substring(url.lastIndexOf(".") );
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

    public static String fileNameWithExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf("/") == -1) {
            return url;
        } else {
            String name = url.substring(url.lastIndexOf("/")  + 1);
            return name;
        }
    }

    public static String fileNameWithoutExtFromUrl(String url) {

        String fullname = fileNameWithExtFromUrl(url);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

    public static String fileNameWithExtFromPath(String path) {

        if (path.lastIndexOf("/") > -1)
            return path.substring(path.lastIndexOf("/") + 1);

        return path;
    }

    public static String fileNameWithoutExtFromPath(String path) {

        String fullname = fileNameWithExtFromPath(path);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }
}
