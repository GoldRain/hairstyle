package com.sinmobile.gogorgeos.fragment.UserProfile;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.RatingListAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.model.RatingModel;

import java.util.ArrayList;

public class UserReviewFragment extends Fragment {

    Context _context;
    MainActivity _activity;
    View view;

    ListView lst_contain;
    RatingListAdapter _adapter;

    ArrayList<RatingModel> _ratings = new ArrayList<>();

    public UserReviewFragment(Context context) {
        // Required empty public constructor

        _context = context;
        _activity = (MainActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_user_rating  , container, false);


        _ratings = Commons.userprofile.get_user_ratings();
        loadLayout();
        return view;
    }

    private void loadLayout() {

        lst_contain = (ListView)view.findViewById(R.id.lst_user_contain);
        _adapter = new RatingListAdapter(_activity, _ratings);
        lst_contain.setAdapter(_adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

}
