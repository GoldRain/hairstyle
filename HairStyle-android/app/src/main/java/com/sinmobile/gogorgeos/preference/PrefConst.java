package com.sinmobile.gogorgeos.preference;

/**
 * Created by GoldRain on 9/25/2016.
 */
public class PrefConst {


    public static final String PREFKEY_USERID  = "user_id";

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_XMPPID = "XMPPID";
    public static final String PREFKEY_USERPWD = "password";
    public static final String PREFKEY_ISREGISTERED = "is_registered";
    public static final String PREFKEY_LASTLOGINID = "lastlogin_id";

    public static final String PREFKEY_CURRNETLANG = "current_language";

}
