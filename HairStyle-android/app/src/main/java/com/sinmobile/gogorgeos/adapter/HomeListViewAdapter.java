package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.model.NewsModel;
import com.sinmobile.gogorgeos.model.TrainingModel;
import com.sinmobile.gogorgeos.model.UserModel;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/21/2017.
 */

public class HomeListViewAdapter extends BaseAdapter {

    MainActivity _activity;
    private static final int TYPE_COMMON = 0;
    private static final int TYPE_NEWS = 1;
    private static final int TYPE_TRAINING = 2;

    ArrayList<Object>_homeobject = new ArrayList<>();

    RecyclerView ui_recyclerImage ;
    HomeRecyclerViewAdapter _adapter_common;
    NewsRecyclerViewAdapter _adapter_news;
    TrainRecyclerAdapter _adapter_train;


   public HomeListViewAdapter (MainActivity activity, ArrayList<Object> homeobject){

        _activity = activity;
       _homeobject = homeobject;

    }

    @Override
    public int getCount() {
        return _homeobject.size();
        /*return 5;*/
    }

    @Override
    public Object getItem(int position) {
        return _homeobject.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0 || position == 1 || position == 2){

            return TYPE_COMMON;
        } else if (position == 3){
            return TYPE_NEWS;
        } else if (position == 4){
            return TYPE_TRAINING;
        }
        return TYPE_COMMON;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        switch (type) {

            case TYPE_COMMON: {

                ListHolder listHolder;

                if (convertView == null) {

                   /* if(position == 0 || position == 1|| position == 2 ){*/
                    listHolder = new ListHolder();

                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_home_list, parent, false);

                    listHolder.txv_Name = (TextView) convertView.findViewById(R.id.title_home);
                    listHolder.txv_viewAll = (TextView) convertView.findViewById(R.id.txv_viewAll);
                    listHolder.recyclerView = (RecyclerView) convertView.findViewById(R.id.rcv_list_home);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(_activity, LinearLayoutManager.HORIZONTAL, false);
                    listHolder.recyclerView.setLayoutManager(linearLayoutManager);

                    convertView.setTag(listHolder);

                } else {
                    listHolder = (ListHolder) convertView.getTag();
                }

                final ArrayList<UserModel> user = (ArrayList<UserModel>) _homeobject.get(position);

                if (user.get(position).get_user_role() == 1){

                    listHolder.txv_Name.setText(_activity.getString(R.string.artists));
                } else if (user.get(position).get_user_role() == 2){

                    listHolder.txv_Name.setText(_activity.getString(R.string.stylists));
                } else if (user.get(position).get_user_role() == 3){
                    listHolder.txv_Name.setText(_activity.getString(R.string.models));
                }

                _adapter_common = new HomeRecyclerViewAdapter(_activity, user);
                listHolder.txv_viewAll.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        if (user.get(position).get_user_role() == 1){

                            _activity.navigate(2);
                        } else if (user.get(position).get_user_role() == 2){

                            _activity.navigate(3);
                        } else if (user.get(position).get_user_role() == 3){
                            _activity.navigate(4);
                        }

                    }
                });
                listHolder.recyclerView.setAdapter(_adapter_common);
            }
            break;

            case TYPE_NEWS: {

                NewsHolder newsHolder;

                if (convertView == null) {

                    newsHolder = new NewsHolder();

                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_home_news, parent, false);
                    newsHolder.txv_Name_new = (TextView) convertView.findViewById(R.id.title_home1);
                    newsHolder.txv_viewAll_new = (TextView) convertView.findViewById(R.id.txv_viewAll1);
                    newsHolder.recyclerView_new = (RecyclerView) convertView.findViewById(R.id.rcv_list_home1);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(_activity, LinearLayoutManager.HORIZONTAL, false);
                    newsHolder.recyclerView_new.setLayoutManager(linearLayoutManager);
                    convertView.setTag(newsHolder);

                } else {
                    newsHolder = (NewsHolder) convertView.getTag();
                }

                final ArrayList<NewsModel> news = (ArrayList<NewsModel>)_homeobject.get(position);

                _adapter_news = new NewsRecyclerViewAdapter(_activity, news);

                newsHolder.txv_Name_new.setText(_activity.getString(R.string.news));
                newsHolder.txv_viewAll_new.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                         _activity.navigate(6);
                    }
                });

                newsHolder.recyclerView_new.setAdapter(_adapter_news);
            }
            break;

            case TYPE_TRAINING: {

                TrainingsHodler trainingsHolder;

                if (convertView == null) {

                    trainingsHolder = new TrainingsHodler();
                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_home_trainings, parent, false);
                    trainingsHolder.txv_Name_tran = (TextView) convertView.findViewById(R.id.title_home_tran);
                    trainingsHolder.txv_viewAll_tran = (TextView) convertView.findViewById(R.id.txv_viewAll1_tran);
                    trainingsHolder.recyclerView_tran = (RecyclerView) convertView.findViewById(R.id.rcv_list_home_train);
                    trainingsHolder.txv_Name_tran.setText(_activity.getString(R.string.training_courses));
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(_activity, LinearLayoutManager.HORIZONTAL, false);
                    trainingsHolder.recyclerView_tran.setLayoutManager(linearLayoutManager);
                    convertView.setTag(trainingsHolder);

                } else {
                    trainingsHolder = (TrainingsHodler) convertView.getTag();
                }

                final ArrayList<TrainingModel> train = (ArrayList<TrainingModel>) _homeobject.get(position);

                _adapter_train = new TrainRecyclerAdapter(_activity, train);
                trainingsHolder.txv_viewAll_tran.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        _activity.navigate(5);
                    }
                });
                trainingsHolder.recyclerView_tran.setAdapter(_adapter_train);
            }
            break;
        }

        return convertView;

    }


    public class ListHolder {

        TextView txv_Name ;
        TextView txv_viewAll;
        RecyclerView recyclerView;
    }

    public class NewsHolder{

        TextView txv_Name_new ;
        TextView txv_viewAll_new;
        RecyclerView recyclerView_new;
    }

    public class TrainingsHodler{

        TextView txv_Name_tran ;
        TextView txv_viewAll_tran;
        RecyclerView recyclerView_tran;
    }

}
