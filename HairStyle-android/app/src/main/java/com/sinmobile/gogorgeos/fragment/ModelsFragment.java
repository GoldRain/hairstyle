package com.sinmobile.gogorgeos.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.UserRoleGridViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.parses.RoleModelPaser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ModelsFragment extends Fragment {

    MainActivity _activity_model;
    ImageView ui_imvBack;
    View view;
    GridView grv_model;
    UserRoleGridViewAdapter _adapter_model;
    ArrayList<UserModel> _models = new ArrayList<>();

    String url = "";

    public ModelsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_models, container, false);
        Commons.ROLE_STATE = 3;

        //loadLayout();
        getModels();

        return view;
    }

    private void getModels(){

        Log.d("**********Param***", String.valueOf(Commons.SEARCH_PARAM));


        _activity_model.showProgress();

        if (Commons.SEARCH_PARAM == 1){

            url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCH_NANE;

        }else if (Commons.SEARCH_PARAM == 2){

            url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCH_DATE;

        }else if (Commons.SEARCH_PARAM == 3){

            url = ReqConst.SERVER_URL + ReqConst.REQ_SEARCH_ADDRESS;

        } else url = ReqConst.SERVER_URL + ReqConst.REQ_GETUSERS;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseModels(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity_model.closeProgress();
                _activity_model.showAlertDialog(_activity_model.getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {


                    if (Commons.SEARCH_PARAM == 1){
                        _activity_model.initParam(0);

                        params.put(ReqConst.PARAM_NAME, Commons.SEARCH_EDIT);

                    }else if (Commons.SEARCH_PARAM == 2){
                        _activity_model.initParam(0);

                        params.put(ReqConst.PARAM_YEAR, String.valueOf(Commons.YEAR));
                        params.put(ReqConst.PARAM_MONTH, Commons.MONTH);
                        params.put(ReqConst.PARAM_VALUE, Commons.VALUE);
                        params.put(ReqConst.PARAM_ADDRESS, Commons.SEARCH_EDIT);

                    }else if (Commons.SEARCH_PARAM == 3){

                        _activity_model.initParam(0);
                        params.put(ReqConst.PARAM_ADDRESS, Commons.SEARCH_EDIT);}

                    params.put(ReqConst.PARAM_USERROLE, String.valueOf(3));

                } catch (Exception e) {

                    _activity_model.closeProgress();
                    _activity_model.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseModels(String response){

        Log.d("Response_Model", response);


        try {
            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);

            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                _activity_model.closeProgress();

                JSONArray models = object.getJSONArray(ReqConst.RES_USERS);

                for (int i = 0; i < models.length(); i++){

                    UserModel artist_user = new UserModel();

                    JSONObject json_model = models.getJSONObject(i);

                    RoleModelPaser model_role = new RoleModelPaser();

                    model_role.parseJson(json_model);

                    while (!model_role.is_isFinish()){}

                    _models.add(model_role.get_user());

                    //Log.d("aaaaaaaaaa", _artists.get(i).get_user_photoUrl());
                }

                loadLayout();

            } else {
                _activity_model.closeProgress();
                _activity_model.showAlertDialog(result_message);
            }

        } catch (JSONException e) {

            _activity_model.closeProgress();
            _activity_model.showAlertDialog(_activity_model.getString(R.string.error));
            e.printStackTrace();
        }
    }

    private void loadLayout() {

        ui_imvBack = (ImageView)_activity_model.findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity_model.onBackPressed();
                Commons.ROLE_STATE = 0;
            }
        });

        grv_model = (GridView)view.findViewById(R.id.grv_model);
        _adapter_model = new UserRoleGridViewAdapter(_activity_model, _models);
        grv_model.setAdapter(_adapter_model);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity_model = (MainActivity) context;
    }


}
