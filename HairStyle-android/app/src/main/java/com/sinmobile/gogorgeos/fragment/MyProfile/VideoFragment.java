package com.sinmobile.gogorgeos.fragment.MyProfile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.VideoGridViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.VideoModel;
import com.sinmobile.gogorgeos.utils.BitmapUtils;
import com.sinmobile.gogorgeos.utils.CustomMultipartRequest;
import com.sinmobile.gogorgeos.utils.MediaRealPathUtil;
import com.sinmobile.gogorgeos.utils.MultiPartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.sinmobile.gogorgeos.commons.Constants.PICK_FROM_VIDEO_GALLERY;


public class VideoFragment extends Fragment implements View.OnClickListener{

    MainActivity _activity;
    View view;
    ArrayList<VideoModel> _videoModels = new ArrayList<>();
    ArrayList<String> _videoPath = new ArrayList<>();
    ArrayList<String> _thumnailPath = new ArrayList<>();

    VideoModel _video_model = new VideoModel();

    GridView gdv_video;
    VideoGridViewAdapter _adapter;
    FloatingActionButton flt_add;

    private Uri _imageCaptureUri, _videoCaptureUri;
    String _thumPath, videoPath = "", _thumbPath = "";
    int _video_id = 0;
    String _video_thumb = "";

    public VideoFragment(Context _context) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_video, container, false);

        loadLayout();
        return view;
    }
    private void loadLayout() {

        flt_add = (FloatingActionButton)view.findViewById(R.id.flt_add_video);
        flt_add.setOnClickListener(this);


        gdv_video = (GridView)view.findViewById(R.id.gdv_video);
        _adapter = new VideoGridViewAdapter(_activity);
        gdv_video.setAdapter(_adapter);

        if (Commons.g_user != null){
        _videoModels = Commons.g_user.get_user_video();
        }

        _adapter.setVideo(_videoModels);
        _adapter.notifyDataSetChanged();

        cameraSetting();
    }

    private void cameraSetting() {

        MediaRecorder mediaRecorder =  new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
        mediaRecorder.setVideoSize(640, 480);
        mediaRecorder.setVideoFrameRate(10);
    }


    private void selectVideo() {

        final String[] items = {getString(R.string.take_video), getString(R.string.take_gallery), getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    takeVideo();

                } else if (item == 1) {
                    takeVideoGallary();

                } else {
                    return;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void takeVideo() {

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,  BitmapUtils.getVideoThumbFolderPath());
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
        startActivityForResult(intent,Constants.PICK_FROM_VIDEO);
    }

    public void takeVideoGallary()
    {
        Intent intent;
        if(android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
        {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        }
        else
        {
            intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI);
        }
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(intent,PICK_FROM_VIDEO_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case PICK_FROM_VIDEO_GALLERY:

                if (resultCode == RESULT_OK) {

                    if (data.getData() != null) {

                        Uri w_uri = data.getData();

                        String w_strPath = MediaRealPathUtil.getPath(_activity, w_uri);

                        String filename = Commons.fileNameWithExtFromUrl(w_strPath);

                        if (w_strPath == null) {
                            _activity.showToast(getString(R.string.getvideo_fail));
                            return;
                        }

                        videoPath = w_strPath;
                        getThumbnail(w_strPath);

                        uploadVideo1();

                        Log.d("=====videoPath=====>", videoPath);

                    } else {
                        _activity.showToast("Failed to select video");
                    }
                }
                break;

            case Constants.PICK_FROM_VIDEO:

                if (resultCode == RESULT_OK) {

                    Uri w_uri = data.getData();

                    String w_strPath = MediaRealPathUtil.getPath(_activity, w_uri);

                    String filename = Commons.fileNameWithExtFromUrl(w_strPath);

                    if (w_strPath == null) {
                        _activity.showToast(getString(R.string.getvideo_fail));
                        return;
                    }

                    videoPath = w_strPath;
                    getThumbnail(w_strPath);

                    uploadVideo1();

                    Log.d("take_video===", videoPath);
                }
                break;
        }
    }

    private void getThumbnail(String _videoPath){

        File saveFile = BitmapUtils.getOutputMediaFile(_activity);

        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(_videoPath, MediaStore.Images.Thumbnails.MINI_KIND);

        _thumPath = saveFile.getAbsolutePath();

        _thumbPath = getThumPath(thumb);

        Log.d("Thumbnail========>", _thumbPath);

    }

    private String getThumPath(Bitmap thumb) {

        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/Temp";
        File dir = new File(file_path);
        if(!dir.exists())
            dir.mkdirs();

        File file = new File(dir, "temp_ex" + ".png");

        try {

            FileOutputStream fOut = new FileOutputStream(file);

            thumb.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }

    public void uploadVideo1() {

        _activity.showProgress();

        try {

            File file = new File(_thumbPath);

            Map<String, String> params = new HashMap<>();
            params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_user_id()));
            params.put(ReqConst.PARAM_VIDEO_COMMENT, "comment");

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADVIDEO1;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    _activity.closeProgress();
                    _activity.showToast("Thumbnail registration failed");
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    parseThumbnail(json);
                }
            }, file, ReqConst.PARAM_VIDEO_THUMBNAILIMAGE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            GogorgeosApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.thumb_upload_fail));
        }
    }

    private void parseThumbnail(String json){

        Log.d("Vidoe_reponse==", json);

        try {
            JSONObject object = new JSONObject(json);

            String result_message = object.getString(ReqConst.RES_MESSAGE);
            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                _video_id = object.getInt(ReqConst.PARAM_ID);
                _video_thumb = object.getString(ReqConst.PARAM_VIDEO_THUMBNAILURL);

                uploadVideo2();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void uploadVideo2(){

        try {

            File file = new File(videoPath);

            Map<String, String> params = new HashMap<>();
            params.put(ReqConst.PARAM_VIDEO_ID, String.valueOf(_video_id));
            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADVIDEO2;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.video_upload_fail));
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    parseUploadFile(json);
                }
            }, file, ReqConst.PARAM_VIDEO_VIDEO, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(60000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            GogorgeosApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.video_upload_fail));
        }
    }

    private void parseUploadFile(String json) {

        Log.d("===Pase==Video", json);

        _activity.closeProgress();

        try {

            JSONObject response =  new JSONObject(json);

            String result_message = response.getString(ReqConst.RES_MESSAGE);

            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                String video_url = response.getString(ReqConst.PARAM_VIDEO_URL);

                _video_model.set_video_id(_video_id);
                _video_model.set_video_url(video_url);
                _video_model.set_video_thumbimageurl(_video_thumb);
                _video_model.set_video_comment("comment");

                _videoModels.add(_video_model);
                Commons.g_user.set_user_video(_videoModels);

                _activity.MyProfileFragment();

                Toast.makeText(_activity, "Your video is uploaded to server.", Toast.LENGTH_LONG).show();

            }else {
                _activity.showAlertDialog(getString(R.string.error));
            }

        }catch (JSONException e){}
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.flt_add_video:
                selectVideo();
                break;
        }
    }
}






/*

    private void uploadVideo() {

        if (videoPath.length()>0){

            _activity.showProgress();

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADVIDEO;

            Log.d("URL_+++VIDEO", url);

            Map<String, String> mHeaderPart= new HashMap<>();
            mHeaderPart.put("Content-type", "multipart/form-data;");
            //File part
            Map<String, File> mFilePartData= new HashMap<>();

            mFilePartData.put(ReqConst.PARAM_VIDEO_VIDEO, new File(videoPath));
            mFilePartData.put(ReqConst.PARAM_VIDEO_THUMBNAIL, new File(_thumbPath));

            Map<String, String> params = new HashMap<>();
            params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_user_id()));
            params.put(ReqConst.PARAM_VIDEO_COMMENT, "");
            Log.d("bbbbbbbb", "bbbbbbbbbbbb");

            CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, _context, url, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject jsonObject) {
                    Log.d("aaaaaaaa", "aaaaaaaaaaaa");
                    parseUploadFile(jsonObject.toString());

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    Log.d("volleyerror====", String.valueOf(volleyError));
                    _activity.showToast(getString(R.string.error));
                    _activity.closeProgress();
                    *//*_isSaving = false;*//*
                }
            }, mFilePartData, params, mHeaderPart);

            mCustomRequest.setRetryPolicy(new DefaultRetryPolicy(60000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            GogorgeosApplication.getInstance().addToRequestQueue(mCustomRequest, url);


        }else {

            Toast.makeText(_activity, "Your alert is uploaded to server.", Toast.LENGTH_LONG).show();
            _activity.closeProgress();
        }
    }

    */
