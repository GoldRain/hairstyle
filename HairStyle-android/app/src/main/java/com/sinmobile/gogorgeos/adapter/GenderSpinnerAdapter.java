package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;

/**
 * Created by ToSuccess on 1/22/2017.
 */

public class GenderSpinnerAdapter extends BaseAdapter {

    private final Context context;

    String[] GenderState;

    public GenderSpinnerAdapter(Context activity) {
        this.context = activity;

        GenderState = context.getResources().getStringArray(R.array.GenderState);
    }

    @Override
    public int getCount() {
        return GenderState.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_gender_spin, parent, false);

        TextView ui_txv_name = (TextView)convertView.findViewById(R.id.txv_state);

        if(position==0){
            ui_txv_name.setText(GenderState[0].trim());
            ui_txv_name.setTextColor(context.getResources().getColor(R.color.hint_color));

        }else {
            ui_txv_name.setText(GenderState[position].trim());
        }
        //Constants.publicstatus=ui_txv_name.getText().toString();

        return convertView;
    }
}
