package com.sinmobile.gogorgeos.utils;

/**
 * Created by HGS on 12/20/2015.
 */

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;


public class PhotoSelectDialog extends Dialog {

    private TextView ui_txvCamera, ui_txvAlbum, ui_txvDelete;
    private Button ui_btnCancel;

    private View.OnClickListener cameraListener = null;
    private View.OnClickListener albumListener = null;
    private View.OnClickListener deleteListener = null;
    private View.OnClickListener cancelListener = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.alert_photo_select);

        ui_txvCamera = (TextView)findViewById(R.id.txv_camera);
        ui_txvAlbum = (TextView)findViewById(R.id.txv_album);
        ui_txvDelete = (TextView) findViewById(R.id.txv_delete);
        ui_btnCancel = (Button)findViewById(R.id.btn_alertCancel);

        LinearLayout lytDelete = (LinearLayout) findViewById(R.id.lyt_delete);


        ui_txvCamera.setOnClickListener(cameraListener);
        ui_txvAlbum.setOnClickListener(albumListener);

        if (deleteListener != null) {
            ui_txvDelete.setOnClickListener(deleteListener);
        } else {
            lytDelete.setVisibility(View.GONE);
        }

        ui_btnCancel.setOnClickListener(cancelListener);

    }

    public PhotoSelectDialog(Context context, View.OnClickListener cameraListener,
                             View.OnClickListener albumListener, View.OnClickListener cancelListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        this.cameraListener = cameraListener;
        this.albumListener = albumListener;
        this.cancelListener = cancelListener;
    }

    public PhotoSelectDialog(Context context, View.OnClickListener cameraListener,
                             View.OnClickListener albumListener, View.OnClickListener deleteListener, View.OnClickListener cancelListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        this.cameraListener = cameraListener;
        this.albumListener = albumListener;
        this.deleteListener = deleteListener;
        this.cancelListener = cancelListener;
    }
}
