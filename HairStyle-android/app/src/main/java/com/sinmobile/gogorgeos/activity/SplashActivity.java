package com.sinmobile.gogorgeos.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Constants;

public class SplashActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        loadMainActivity();
    }

    private void loadMainActivity(){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0,0);


            }
        }, Constants.SPLASH_TIME);
    }
}
