package com.sinmobile.gogorgeos.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.base.CommonActivity;

public class NewPWDActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    EditText ui_edtNewPwd, ui_edtConPwd;
    TextView ui_txvSubmit;
    ForgetActivity f;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pwd);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView) findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_edtNewPwd = (EditText)findViewById(R.id.edt_newpassword);
        ui_edtConPwd = (EditText)findViewById(R.id.edt_confirm_pwd);

        ui_txvSubmit = (TextView)findViewById(R.id.txv_submit);
        ui_txvSubmit.setOnClickListener(this);

        // container
        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.activity_new_pwd);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtNewPwd.getWindowToken(), 0);
                return false;
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                gotoVerification();
                break;

            case R.id.txv_submit:
                gotoMain();
                break;
        }
    }

    private void gotoVerification(){

        f._forget = true;
        Intent intent = new Intent(this, VerificationActivity.class);
        overridePendingTransition(0,0);
        startActivity(intent);
        finish();
    }

    private void gotoMain(){

        Intent intent = new Intent(this, MainActivity.class);
        overridePendingTransition(0,0);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoVerification();
    }
}
