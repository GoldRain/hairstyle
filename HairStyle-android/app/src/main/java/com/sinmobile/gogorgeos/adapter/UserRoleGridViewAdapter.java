package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.customview.SquareImageView;
import com.sinmobile.gogorgeos.model.UserModel;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 1/21/2017.
 */

public class UserRoleGridViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<UserModel> _users = new ArrayList<>();

    public UserRoleGridViewAdapter(MainActivity activity, ArrayList<UserModel> users){

        _activity = activity;

        _users.clear();
        _users = users;
    }
    @Override
    public int getCount() {
        return _users.size();
    }

    @Override
    public Object getItem(int position) {
        return _users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ArtistsHolder artistsHolder;

        if (convertView == null){

            artistsHolder = new ArtistsHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_artist_grv, parent, false);

            artistsHolder.imv_artist = (SquareImageView)convertView.findViewById(R.id.imv_artist);
            artistsHolder.txv_name = (TextView)convertView.findViewById(R.id.txv_name_art);

            convertView.setTag(artistsHolder);

        } else {

          artistsHolder = (ArtistsHolder)convertView.getTag();
        }

        final UserModel user = _users.get(position);

        Glide.with(_activity).load(user.get_user_photoUrl()).placeholder(R.drawable.bg_non_profile).into(artistsHolder.imv_artist);
        artistsHolder.txv_name.setText(user.get_user_firstname());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("========Useradress====>", user.get_user_adress());
                Log.d("========UserPhone===>", user.get_user_phonenumber());
                Log.d("====AVAILABLE==>", String.valueOf(user.get_user_available().size()));

                if (Commons.g_user == null){

                    _activity.UserProfile(user);

                } else if (Commons.g_user.get_user_id() != user.get_user_id()){

                        _activity.UserProfile(user);
                    }



            }
        });

        return convertView;
    }

    public class ArtistsHolder {

        public SquareImageView imv_artist;
        public TextView txv_name;
    }
}
