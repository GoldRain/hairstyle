package com.sinmobile.gogorgeos.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Constants;

public class ForgetActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    EditText ui_edtEmaiId;
    TextView ui_txvSubmit, ui_txvCancel;

    public static boolean _forget = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget);

        loadLayout();
    }

    private void loadLayout() {

        ui_imvBack = (ImageView) findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_edtEmaiId = (EditText)findViewById(R.id.edt_emailId);

        ui_txvSubmit = (TextView)findViewById(R.id.txv_submit);
        ui_txvSubmit.setOnClickListener(this);

        ui_txvCancel = (TextView)findViewById(R.id.txv_cancel);
        ui_txvCancel.setOnClickListener(this);

        // container
        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.activity_forget);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtEmaiId.getWindowToken(), 0);
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                gotoLogin();
                break;

            case R.id.txv_submit:
                _forget = true;
                gotoVerification();
                break;

            case R.id.txv_cancel:
                gotoLogin();
                break;
        }
    }

    private void gotoVerification(){

        Intent intent = new Intent(this, VerificationActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoLogin(){

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoLogin();
    }
}
