package com.sinmobile.gogorgeos.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sinmobile.gogorgeos.fragment.MyProfile.ImageFragment;
import com.sinmobile.gogorgeos.fragment.MyProfile.ReviewFragment;
import com.sinmobile.gogorgeos.fragment.MyProfile.VideoFragment;

/**
 * Created by ToSuccess on 1/20/2017.
 */

public class MyProfileViewPagerAdapter extends FragmentStatePagerAdapter {

    Context _context;
    public MyProfileViewPagerAdapter(FragmentManager fm , Context context) {
        super(fm);
        _context = context;
    }


    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }


    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null ;
        switch (position){
            case 0:

                fragment = new ImageFragment(_context);
                break;
            case 1:
                fragment = new VideoFragment(_context);
                break;
            case 2:
                fragment = new ReviewFragment(_context);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
