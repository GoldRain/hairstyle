package com.sinmobile.gogorgeos.fragment.MyProfile;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.ImageGridViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.customview.SquareImageView;
import com.sinmobile.gogorgeos.utils.BitmapUtils;
import com.sinmobile.gogorgeos.utils.MultiPartRequest;
import com.sinmobile.gogorgeos.utils.PhotoSelectDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.sinmobile.gogorgeos.activity.MainActivity.MY_PEQUEST_CODE;


public class ImageFragment extends Fragment {

    Context _context;
    MainActivity _activity;
    GridView gdv_image;
    ImageGridViewAdapter _adapter;
    FloatingActionButton flt_add;

    MyProfileFragment myfragment;

    ArrayList<String> _images = new ArrayList<>();
    View view;

    private PhotoSelectDialog _photoSelectDialog;
    Uri _imageCaptureUri;
    String _photoPath = "";
    SquareImageView ui_imvMyPhoto;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};



    public ImageFragment(Context context) {
        _context = context;
        _activity = (MainActivity)getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_image, container, false);

        if (Commons.g_user != null) {

            _images = Commons.g_user.get_user_images();
        }


        checkAllPermission();
        loadLayout();

        return view;
    }

    /*==================== CARMERA Permission========================================*/
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(_activity, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(_activity, PERMISSIONS, 101);
        }
    }

    /*==================== CARMERA Permission========================================*/
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
      /*////////////////////////////////////////////////////////////////*/

    private void loadLayout() {

        ui_imvMyPhoto = (SquareImageView)view.findViewById(R.id.imv_image);

        gdv_image = (GridView)view.findViewById(R.id.grd_image);
        _adapter = new ImageGridViewAdapter(_activity);
        gdv_image.setAdapter(_adapter);

        _adapter.setImages(_images);

        flt_add = (FloatingActionButton)view.findViewById(R.id.flt_add);

        flt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!hasPermissions(_activity, PERMISSIONS)){

                    ActivityCompat.requestPermissions(_activity, PERMISSIONS, Constants.MY_PEQUEST_CODE);
                }else {

                    setMyPhoto();
                }
            }
        });

    }

    /*Take Photo*/

    public void setMyPhoto(){

        View.OnClickListener cameraListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakePhotoAction();


                if (!_activity.hasPermissions(_activity, PERMISSIONS)){

                    ActivityCompat.requestPermissions(_activity, PERMISSIONS, MY_PEQUEST_CODE);
                }else {
                }
                _photoSelectDialog.dismiss();
            }
        };
        View.OnClickListener albumListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doTakeGallery();
                _photoSelectDialog.dismiss();
            }
        };

        View.OnClickListener cancelListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _photoSelectDialog.dismiss();
            }
        };

            _photoSelectDialog = new PhotoSelectDialog(_context, cameraListener, albumListener, cancelListener);

        _photoSelectDialog.show();
    }

    public void doTakePhotoAction(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(_activity);

                        InputStream in = _activity.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        //ui_imvMyPhoto.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                        //_images.add(_photoPath);
                        _adapter.setImages(_images);
                        _adapter.notifyDataSetChanged();

                        uploadImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(_activity, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(_activity)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void uploadImage(){

        //if no profile photo
        if (_photoPath.length() == 0) {
            _activity.closeProgress();
            return;
        }

        try {

            _activity.showProgress();

            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<String, String>();

            params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_user_id()));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADIMAGGE;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    _activity.closeProgress();
                    _activity.showToast("Error");

                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    parseuploadImage(json);
                }
            }, file, ReqConst.PARAM_USERPHOTO, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            GogorgeosApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            _activity.closeProgress();

            _activity.showToast(_activity.getString(R.string.error));
        }
    }

    private void parseuploadImage(String response){

        try {
            JSONObject object = new JSONObject(response);
            String result_message = object.getString(ReqConst.RES_MESSAGE);
            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                _activity.closeProgress();

                String url = object.getString("url");
                _images.add(url);
                Commons.g_user.set_user_images(_images);

                _activity.MyProfileFragment();

                _activity.showToast("Success UploadImage");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void refresh(){_adapter.notifyDataSetChanged();}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }

    @Override
    public void onResume() {
        refresh();
        super.onResume();
    }
}
