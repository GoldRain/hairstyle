package com.sinmobile.gogorgeos.customcalendar.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sinmobile.gogorgeos.customcalendar.fragment.CalendarViewFragment;
import com.sinmobile.gogorgeos.customcalendar.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by joybar on 4/27/16.
 */
public class CalendarViewPagerAdapter extends FragmentStatePagerAdapter {
    public static final int NUM_ITEMS = 12;
    //public static final int NUM_ITEMS_CURRENT = NUM_ITEMS/2;
    public static final int NUM_ITEMS_CURRENT = (DateUtils.getMonth() -1);
    private int mThisMonthPosition = DateUtils.getYear()*12 + DateUtils.getMonth() -1;//---100 -position
    private int number = mThisMonthPosition - NUM_ITEMS_CURRENT;
    private boolean isChoiceModelSingle;

    List<Long> _valuesArray = new ArrayList<>();

    public CalendarViewPagerAdapter(FragmentManager fm,boolean isChoiceModelSingle,  List<Long> valuesArray) {
        super(fm);
        this.isChoiceModelSingle = isChoiceModelSingle;
        this._valuesArray= valuesArray;
    }

    @Override
    public Fragment getItem(int position) {
        int year = getYearByPosition(position);
        int month = getMonthByPosition(position);
        Fragment fragment = CalendarViewFragment.newInstance(year,month,isChoiceModelSingle, _valuesArray);

        return  fragment;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    public int getYearByPosition(int position){
        int year = (position+number)/12;
        return year;
    }
    public  int getMonthByPosition(int position) {
        int month = (position + number) % 12 + 1;
        return month;
    }
}
