package com.sinmobile.gogorgeos.model;

/**
 * Created by ToSuccess on 1/20/2017.
 */

public class RatingModel {


    int _rate_id = 0;                  //sender id
    int _rate_sender = 0;
    int _rate_receiver = 0;             //receiver id
    String _rate_comment = "";           //reveiw comment
    int _rate_marks = 0;              //review stars

    String _rate_sendername = "";      // sender name
    String _rate_senderphoto = "";  // sender photo url


    public int get_rate_id() {
        return _rate_id;
    }

    public void set_rate_id(int _rate_id) {
        this._rate_id = _rate_id;
    }

    public int get_rate_receiver() {
        return _rate_receiver;
    }

    public void set_rate_receiver(int _rate_receiver) {
        this._rate_receiver = _rate_receiver;
    }

    public String get_rate_senderphoto() {
        return _rate_senderphoto;
    }

    public void set_rate_senderphoto(String _rate_senderphoto) {
        this._rate_senderphoto = _rate_senderphoto;
    }

    public int get_rate_sender() {
        return _rate_sender;
    }

    public void set_rate_sender(int _rate_sender) {
        this._rate_sender = _rate_sender;
    }

    public String get_rate_comment() {
        return _rate_comment;
    }

    public void set_rate_comment(String _rate_comment) {
        this._rate_comment = _rate_comment;
    }

    public int get_rate_marks() {
        return _rate_marks;
    }

    public void set_rate_marks(int _rate_marks) {
        this._rate_marks = _rate_marks;
    }

    public String get_rate_sendername() {
        return _rate_sendername;
    }

    public void set_rate_sendername(String _rate_sendername) {
        this._rate_sendername = _rate_sendername;
    }
}
