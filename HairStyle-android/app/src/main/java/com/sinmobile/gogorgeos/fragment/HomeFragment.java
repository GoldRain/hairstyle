package com.sinmobile.gogorgeos.fragment;

import android.content.Context;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ViewFlipper;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.adapter.HomeListViewAdapter;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.model.AboutUsModel;
import com.sinmobile.gogorgeos.model.FaqModel;
import com.sinmobile.gogorgeos.model.NewsModel;
import com.sinmobile.gogorgeos.model.TrainingModel;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.parses.HomeModelParser;
import com.sinmobile.gogorgeos.preference.PrefConst;
import com.sinmobile.gogorgeos.preference.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.sinmobile.gogorgeos.commons.Commons.ADDRESS;


public class HomeFragment extends Fragment {


    ArrayList<Object> _homeobject = new ArrayList<>();
    ArrayList<String> _advertisements = new ArrayList<>();
    ArrayList<UserModel> _artists = new ArrayList<>();
    ArrayList<UserModel> _styles = new ArrayList<>();
    ArrayList<UserModel> _models = new ArrayList<>();
    ArrayList<NewsModel> _news = new ArrayList<>();
    ArrayList<TrainingModel> _trainings = new ArrayList<>();
    ArrayList<FaqModel> _faqs = new ArrayList<>();
    ArrayList<AboutUsModel> _abouts = new ArrayList<>();

    MainActivity _activity;
    View view;
    ExpandableHeightListView listcontainer;


    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    ViewFlipper mHomeflipper;
    ImageLoader _imageloader;
    private Animation.AnimationListener mAnimationListener;

    HomeListViewAdapter homelistviewadapter;

    private final GestureDetector _detector = new GestureDetector(new SwipeGestureDetector());

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        loadLayout();

        ADDRESS = new ArrayList<>();

        getHomeData();
        return view;
    }

    public void getHomeData(){

        _activity.showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETHOMEDATA;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    Log.d("_language===>",String.valueOf(Preference.getInstance().getValue(_activity, PrefConst.PREFKEY_CURRNETLANG,0)) );

                    params.put(ReqConst.PARAM_USER_LANGUAGE, String.valueOf(Preference.getInstance().getValue(_activity, PrefConst.PREFKEY_CURRNETLANG,0)));

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void parseResponse(String response){

        Log.d("******GETHOME_DATE*****", response);

        try {
            JSONObject object = new JSONObject(response);

           // _activity.closeProgress();

            String message = object.getString(ReqConst.RES_MESSAGE);
            if (message.equals(ReqConst.CODE_SUCCESS)){

                JSONObject homeDate = object.getJSONObject(ReqConst.RES_HOMEINFO);

                /*/////////////////////////ADVERTIS ////////////////////*/

                JSONArray advertisments = homeDate.getJSONArray(ReqConst.RES_ADVERTISEMENT);
                for (int i = 0; i < advertisments.length(); i++){

                    String json_advertisment = (String)advertisments.get(i);
                    _advertisements.add(json_advertisment);
                }

                /*/////////////////////////ARTISTS ////////////////////*/
                JSONArray artists = homeDate.getJSONArray(ReqConst.RES_ARTISTS);
                for (int j = 0; j < artists.length(); j++) {

                    JSONObject json_artists = (JSONObject) artists.get(j);

                    HomeModelParser artist_parser = new HomeModelParser();
                    artist_parser.parseJson(json_artists);

                    while (!artist_parser.is_isFinish()){}

                    _artists.add(artist_parser.get_user());
                }

                _homeobject.add( _artists);

                /*/////////////////////////STYLISTS ////////////////////*/
                JSONArray stylists = homeDate.getJSONArray(ReqConst.RES_STYLISTS);
                for (int k = 0; k < stylists.length(); k++) {

                    JSONObject json_stylists = (JSONObject) stylists.get(k);

                    HomeModelParser style_parser = new HomeModelParser();
                    style_parser.parseJson(json_stylists);

                    while (!style_parser.is_isFinish()){}

                    _styles.add(style_parser.get_user());
                }
                _homeobject.add( _styles);

                /*/////////////////////////MODELS ////////////////////*/

                JSONArray models = homeDate.getJSONArray(ReqConst.RES_MODELS);
                for (int m = 0; m < models.length(); m++) {

                    JSONObject json_models = (JSONObject) models.get(m);

                    HomeModelParser model_parser = new HomeModelParser();
                    model_parser.parseJson(json_models);

                    while (!model_parser.is_isFinish()){}

                    _models.add(model_parser.get_user());
                }
                _homeobject.add( _models);

                /*/////////////////////////NEWS ////////////////////*/

                JSONArray news = homeDate.getJSONArray(ReqConst.RES_NEWS);
                for (int n = 0; n < news.length(); n++){

                    JSONObject json_news = (JSONObject)news.get(n);

                    NewsModel newsModel = new NewsModel();

                    newsModel.set_news_id(json_news.getInt(ReqConst.PARAM_NEWSID));
                    newsModel.set_news_title(json_news.getString(ReqConst.PARAM_NEWSTITLE));
                    newsModel.set_news_date(json_news.getString(ReqConst.PARAM_NEWSDATE));
                    newsModel.set_news_imageurl(json_news.getString(ReqConst.PARAM_NEWSIMAGEURL));
                    newsModel.set_news_content(json_news.getString(ReqConst.PARAM_NEWSCONTENT));

                    _news.add(newsModel);
                }

                _homeobject.add(_news);

                 /*/////////////////////////TRAINING ////////////////////*/

                JSONArray trainings = homeDate.getJSONArray(ReqConst.RES_TRAININGS);
                for (int p = 0; p < trainings.length(); p++){

                    JSONObject json_trainings = (JSONObject)trainings.get(p);

                    TrainingModel trainingModel = new TrainingModel();

                    //trainingModel.set_training_id(json_trainings.getInt(ReqConst.PARAM_TRAINID));
                    trainingModel.set_training_title(json_trainings.getString(ReqConst.PARAM_TRAINTITLE));
                    trainingModel.set_training_date(json_trainings.getString(ReqConst.PARAM_TRAINDATE));
                    trainingModel.set_training_location(json_trainings.getString(ReqConst.PARAM_TRAINLOCATION));
                    trainingModel.set_training_email(json_trainings.getString(ReqConst.PARAM_TRAINEMAIL));
                    trainingModel.set_training_imageurl(json_trainings.getString(ReqConst.PARAM_TRAINIMAGEURL));
                    trainingModel.set_training_content(json_trainings.getString(ReqConst.PARAM_TRAINCONTENT));
                    trainingModel.set_training_price(json_trainings.getDouble(ReqConst.PARAM_TRAINPRICE));
                    trainingModel.set_training_phonenumber(json_trainings.getString(ReqConst.PARAM_TRAIN_PHONENUMBER));

                    _trainings.add(trainingModel);
                }
                _homeobject.add(_trainings);

                JSONArray address = homeDate.getJSONArray(ReqConst.RES_ADDRESS);
                for (int q = 0; q < address.length(); q++){

                    String json_address = (String)address.get(q);

                    Log.d("addrss", json_address);

                    if (json_address.equals("")){
                        continue;
                    }

                    ADDRESS.add(json_address);
                }
            }

            /*//////////////////////////////////////////////////////////////////////////////////////////*/

            _imageloader = GogorgeosApplication.getInstance().getImageLoader();
            mHomeflipper = (ViewFlipper) view.findViewById(R.id.home_viewflipper);

            for (int i = 0; i < _advertisements.size(); i++) {
                NetworkImageView imageView = new NetworkImageView(_activity);
                imageView.setImageUrl(_advertisements.get(i), _imageloader);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setTag(1000 + i);
                mHomeflipper.addView(imageView);
            }

            mHomeflipper.setAutoStart(true);
            mHomeflipper.setInAnimation(AnimationUtils.loadAnimation(_activity, R.anim.left_in));
            mHomeflipper.setOutAnimation(AnimationUtils.loadAnimation(_activity, R.anim.left_out));
            mHomeflipper.setFlipInterval(3000);
            mHomeflipper.getInAnimation().setAnimationListener(mAnimationListener);

            mHomeflipper.startFlipping();


            mHomeflipper.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    _detector.onTouchEvent(motionEvent);
                    return true;
                }
            });


            // animation listener
            mAnimationListener = new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {
                    Log.d("Flipper", "start");

                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    Log.d("Flipper", "Repeat");
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    Log.d("Flipper", "end");

                }
            };

            homelistviewadapter = new HomeListViewAdapter(_activity,_homeobject );
            listcontainer.setAdapter(homelistviewadapter);

            //FAQ Connect
            getFaq();

        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    private void getFaq(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETALLFAQ;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseFaq(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("", "");

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void parseFaq(String response){

        try {
            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);

            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                JSONArray faq_models = object.getJSONArray(ReqConst.RES_FAQINFO);

                for (int i = 0; i < faq_models.length(); i++){

                    JSONObject json_faq = (JSONObject) faq_models.get(i);
                    FaqModel faqModel = new FaqModel();

                    faqModel.set_id(json_faq.getInt(ReqConst.PARAM_FAQID));
                    faqModel.set_title(json_faq.getString(ReqConst.PARAM_FAQTITLE));
                    faqModel.set_content(json_faq.getString(ReqConst.PARAM_FAQCONTENT));

                    _faqs.add(faqModel);

                }

                Log.d("FAQ=====END", "END");
                Commons.FAQS = _faqs;

                //getAboutUs Connect Server
                getAboutUs();

            } else _activity.showAlertDialog(result_message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void getAboutUs(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETABOUTUS;
        StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseAboutUs(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("", "");

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseAboutUs(String response){

        try {
            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);
            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                JSONArray json_about = object.getJSONArray(ReqConst.RES_ABOUTUSINFO);

                for (int i = 0; i < json_about.length(); i++){

                    JSONObject about = (JSONObject)json_about.get(i);

                    Commons.ABOUTUS_TITLE = about.getString(ReqConst.PARAM_ABOUTUSTITLE);
                    Commons.ABOUTUS_CONTENT = about.getString(ReqConst.PARAM_ABOUTUS_CONTENT);
                }

                Log.d("==AboutUs End==", "END");
                //getContactUs Connecting Server:
                getContactUS();

            } else _activity.showAlertDialog(_activity.getString(R.string.error));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getContactUS(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETCONTACTUS;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseContactUs(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(_activity.getString(R.string.error));
            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put("", "");

                } catch (Exception e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseContactUs(String response){

        _activity.closeProgress();

        try {
            JSONObject object = new JSONObject(response);

            String result_message = object.getString(ReqConst.RES_MESSAGE);
            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                JSONObject contactUs = object.getJSONObject(ReqConst.RES_CONTACTUSINFO);

                Commons.EMAIL = contactUs.getString(ReqConst.PARAM_EMAIL);
                Commons.PHONENUMBER = contactUs.getString(ReqConst.PARAM_PHONENUMBER);

                Log.d("=====ContactUs==END==", "End");

            } else {
                _activity.closeProgress();
                _activity.showAlertDialog(result_message);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void loadLayout() {

        listcontainer=(ExpandableHeightListView) view.findViewById(R.id.lst_container);
        homelistviewadapter = new HomeListViewAdapter(_activity,_homeobject );
        listcontainer.setAdapter(homelistviewadapter);
        listcontainer.setExpanded(true);

    }

    public class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            try {
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY){

                    mHomeflipper.setInAnimation(AnimationUtils.loadAnimation(_activity, R.anim.left_in));
                    mHomeflipper.setOutAnimation(AnimationUtils.loadAnimation(_activity, R.anim.left_out));
                    // controlling animation
                    mHomeflipper.getInAnimation().setAnimationListener(mAnimationListener);
                    mHomeflipper.showNext();

                    return true;

                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                    mHomeflipper.setInAnimation(AnimationUtils.loadAnimation(_activity, R.anim.right_in));
                    mHomeflipper.setOutAnimation(AnimationUtils.loadAnimation(_activity,R.anim.right_out));
                    // controlling animation
                    mHomeflipper.getInAnimation().setAnimationListener(mAnimationListener);
                    mHomeflipper.showPrevious();
                    return true;
                }
            } catch (Exception e){
                e.printStackTrace();
            }

            return false;
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity) context;
    }
}
