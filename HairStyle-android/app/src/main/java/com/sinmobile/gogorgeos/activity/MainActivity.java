package com.sinmobile.gogorgeos.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sinmobile.gogorgeos.GogorgeosApplication;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.adapter.LocationListAdapter;
import com.sinmobile.gogorgeos.adapter.SideMenuAdapter;
import com.sinmobile.gogorgeos.base.CommonActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.commons.Constants;
import com.sinmobile.gogorgeos.commons.ReqConst;
import com.sinmobile.gogorgeos.customcalendar.data.CalendarDate;
import com.sinmobile.gogorgeos.customcalendar.fragment.CalendarViewFragment;
import com.sinmobile.gogorgeos.customcalendar.fragment.CalendarViewPagerFragment;
import com.sinmobile.gogorgeos.fragment.AboutUsFragment;
import com.sinmobile.gogorgeos.fragment.ArtistsFragment;
import com.sinmobile.gogorgeos.fragment.HomeFragment;
import com.sinmobile.gogorgeos.fragment.ModelsFragment;
import com.sinmobile.gogorgeos.fragment.MyProfile.MyProfileEditFragment;
import com.sinmobile.gogorgeos.fragment.MyProfile.MyProfileFragment;
import com.sinmobile.gogorgeos.fragment.News.NewsDetailsFragment;
import com.sinmobile.gogorgeos.fragment.News.NewsListsFragment;
import com.sinmobile.gogorgeos.fragment.StylistsFragment;
import com.sinmobile.gogorgeos.fragment.Training.TrainingDetailsFragment;
import com.sinmobile.gogorgeos.fragment.Training.TrainingFragment;
import com.sinmobile.gogorgeos.fragment.UserProfile.UserDetailsFragment;
import com.sinmobile.gogorgeos.model.AvailableModel;
import com.sinmobile.gogorgeos.model.NewsModel;
import com.sinmobile.gogorgeos.model.RatingModel;
import com.sinmobile.gogorgeos.model.TrainingModel;
import com.sinmobile.gogorgeos.model.UserModel;
import com.sinmobile.gogorgeos.model.VideoModel;
import com.sinmobile.gogorgeos.parses.UserModelParser;
import com.sinmobile.gogorgeos.preference.PrefConst;
import com.sinmobile.gogorgeos.preference.Preference;
import com.sinmobile.gogorgeos.utils.MonthDays;
import com.sinmobile.gogorgeos.utils.PlaceJSONParser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.sinmobile.gogorgeos.commons.Commons.AVA_DAY;
import static com.sinmobile.gogorgeos.commons.Commons.AVA_MONTH;
import static com.sinmobile.gogorgeos.commons.Commons.AVA_YEAR;
import static com.sinmobile.gogorgeos.commons.Commons.screenheight;
import static com.sinmobile.gogorgeos.commons.Commons.screenwidth;

@SuppressWarnings("deprecation")
public class MainActivity extends CommonActivity  implements View.OnClickListener, CalendarViewPagerFragment.OnPageChangeListener,
        CalendarViewFragment.OnDateClickListener, CalendarViewFragment.OnDateCancelListener{

    LocationListAdapter _adapter;
    ListView lst_location;

    String _email = "";
    String _password = "";

    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Bind(R.id.toolbar_)
    Toolbar toolbar;

    @Bind(R.id.imv_call_drawer)
    ImageView imv_call_drawer;

    @Bind(R.id.imv_back)
    ImageView imv_back;

    @Bind(R.id.txv_title)
    TextView txv_title;

    @Bind(R.id.nav_listview)
    ListView menuListView;

    @Bind(R.id.imv_setting)
    ImageView imv_setting;


    ImageView imv_search;


    public static boolean backEnable = false;

    private static final String NAV_ITEM_ID = "naveItemId";
    int _naveItemId;

    public ActionBarDrawerToggle drawerToggle;
    TextView titleTextView;

    //menu
    static Menu menu;
    MenuItem myAccountItem, myProfile;
    public static boolean isVisibleLogout = false;

    public TextView tv_date;
    private List<CalendarDate> mListDate = new ArrayList<>();

    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.RECORD_AUDIO, Manifest.permission.BIND_VOICE_INTERACTION, Manifest.permission.CALL_PHONE};

    public static int MY_PEQUEST_CODE = 123;

    View v;


    ImageView imv_artist;
    ImageView imv_style;
    ImageView imv_model ;

    ImageView imv_calendar;

    TextView txv_search_title;
    EditText edt_search;

    MonthDays _monthdays;

    //================= Autocomplete=======================
    AutoCompleteTextView atvfrompostalstreet;
    PlacesTask placesTask;
    ParserTask parserTask;


    /*/////////////////////////////////////////////////START MAIN//////////////////////////////////////////////////////////////////*/

    @NonNull
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Commons.LANGUAGE = Preference.getInstance().getValue(this, PrefConst.PREFKEY_CURRNETLANG,0);


        // screesize calculate
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();

        int statusbarheight = getStatusBarHeight();

        Log.d("statusbarHeight", String.valueOf(statusbarheight));

        screenwidth = width;
        screenheight = height - statusbarheight/* - 60 * 3 - 3 * 72*/;
        Log.d("width==========>", String.valueOf(width));
        Log.d("Height++++++++==>", String.valueOf(height));
        /*////////////////////End//////////////////////////////*/

        ButterKnife.bind(this);

        toolbar.setTitle("");
        toolbar.setSubtitle("");

        final ArrayList<String> menuItems = new ArrayList<>(Arrays.asList("", getString(R.string.home), getString(R.string.artists), getString(R.string.stylists), getString(R.string.models),getString(R.string.training_courses),
                getString(R.string.news), getString(R.string.faq), getString(R.string.about_us), getString(R.string.contact_us)));

        menuListView.setAdapter(new SideMenuAdapter(this, menuItems));

        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0){
                    gotoLogin();
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    navigate(position);
                }
            }
        });

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer);
        setSupportActionBar(toolbar);

        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        imv_back.setOnClickListener(this);

        titleTextView = (TextView)toolbar.findViewById(R.id.toolbar_title);
        imv_call_drawer.setOnClickListener(this);

        imv_setting.setOnClickListener(this);

        imv_calendar = (ImageView)findViewById(R.id.imv_calendar);
        imv_calendar.setOnClickListener(this);

        imv_search = (ImageView)findViewById(R.id.imv_search);
        imv_search.setOnClickListener(this);


        changeTitle(View.VISIBLE, View.GONE, View.GONE, View.VISIBLE , View.VISIBLE);   //titlebar setting

        currentPage(false, false, false);

        //navigate to select fragment
        navigate(1);
        checkAllPermission();

        String email = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USEREMAIL, "");
        String pwd = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERPWD, "");

        if (email.length() > 0 && pwd.length() > 0) {

            _email = email;
            _password = pwd;

            ProgressLogin();

        } else return;



    }

/////////////////////Here data from Server ://///////////////////////////////////////////////

    private void ProgressLogin(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseLogin(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showAlertDialog(getString(R.string.error));
                closeProgress();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_USEREMAIL, _email);
                    params.put(ReqConst.PARAM_USERPWD, _password);

                } catch (Exception e) {

                    showAlertDialog(getString(R.string.error));
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        GogorgeosApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseLogin(String json){

        try {

            JSONObject object = new JSONObject(json);
            String result_message = object.getString(ReqConst.RES_MESSAGE);

            if (result_message.equals(ReqConst.CODE_SUCCESS)){

                closeProgress();

                Log.d("LOGIN*********>>>", json);
                UserModelParser modelParser = new UserModelParser();
                modelParser.parseJson(object);

                while (!modelParser.is_isFinish()){}

                Commons.g_user =  modelParser.get_user();
                Commons.g_user.set_user_password(_password);

                Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, _password);

                Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, _email);

                String lastLoginEmail = Preference.getInstance().getValue(getApplicationContext(), PrefConst.PREFKEY_LASTLOGINID, "");

                // init database if new user login
                if (!lastLoginEmail.equals(_email)) {
                    Preference.getInstance().put(getApplicationContext(), PrefConst.PREFKEY_LASTLOGINID, _email);
                }
            }

        } catch (Exception e){}
    }
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    //==================== Permission========================================

    public boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }


    }

    //==================================================================


    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);

        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //pass any configuration change to drawer togle
        drawerToggle.onConfigurationChanged(newConfig);
    }


    public void setAppTitle(CharSequence title){txv_title.setText(title);}

    Fragment contentFragment = null;

    public void navigate(final int position){

        backEnable = false;

        FragmentTransaction fragmentTransaction;

        switch (position){

            case 1:
                backEnable = false;
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                setAppTitle(getString(R.string.home));
                changeTitle(View.VISIBLE, View.GONE, View.GONE, View.VISIBLE, View.VISIBLE);          //draw_menu, back_button, calendar_button, search_button, setting_button
                contentFragment = new HomeFragment();
                overridePendingTransition(0,0);
                break;

            case 2:
                backEnable = true;
                changeTitle(View.GONE, View.VISIBLE, View.GONE, View.VISIBLE, View.VISIBLE);          //draw_menu, back_button, calendar_button, search_button
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                setAppTitle(getString(R.string.artists));
                contentFragment = new ArtistsFragment();
                break;

            case 3:
                backEnable = true;
                changeTitle(View.GONE, View.VISIBLE, View.GONE, View.VISIBLE, View.VISIBLE);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                setAppTitle(getString(R.string.stylists));
                contentFragment = new StylistsFragment();
                break;

            case 4:
                backEnable = true;
                changeTitle(View.GONE, View.VISIBLE, View.GONE, View.VISIBLE, View.VISIBLE);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                setAppTitle(getString(R.string.models));
                contentFragment = new ModelsFragment();
                break;

            case 5:
                backEnable = true;
                changeTitle(View.GONE, View.VISIBLE, View.GONE, View.VISIBLE, View.VISIBLE);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                setAppTitle(getString(R.string.training_courses));
                contentFragment = new TrainingFragment();
                break;

            case 6:
                backEnable = true;
                changeTitle(View.GONE, View.VISIBLE, View.GONE, View.VISIBLE, View.VISIBLE);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                setAppTitle(getString(R.string.news_lists));
                contentFragment = new NewsListsFragment();
                break;

            case 7:
                //contentFragment = new FaqFragment();

                Intent intent = new Intent(MainActivity.this, FAQActivity.class);
                startActivity(intent);
                finish();
                break;

            case 8:
                backEnable = true;
                setAppTitle(getString(R.string.about_us));
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                changeTitle(View.GONE, View.VISIBLE, View.GONE, View.GONE, View.VISIBLE);
                contentFragment = new AboutUsFragment();
                break;

            case 9:

                Intent intent1 = new Intent(this, ContactUsActivity.class);
                startActivity(intent1);
                finish();
                break;

            default:
                contentFragment = null;
                break;

        }

        if (contentFragment != null){

            try{

                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frm_container, contentFragment).commit();
                drawerLayout.closeDrawer(menuListView);

            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void MyProfileFragment(){

        backEnable = true;
        changeTitle(View.GONE, View.VISIBLE, View.VISIBLE, View.GONE, View.VISIBLE);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        setAppTitle(getString(R.string.my_profile));

        MyProfileFragment fragment = new MyProfileFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void MyProfileEditFragment(){

        backEnable = true;
        changeTitle(View.GONE, View.VISIBLE, View.GONE, View.GONE, View.VISIBLE);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        setAppTitle(getString(R.string.my_profile));

        MyProfileEditFragment fragment = new MyProfileEditFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();

    }

    public void UserProfile(UserModel userModel){

        backEnable = true;
        changeTitle(View.GONE, View.VISIBLE, View.VISIBLE, View.GONE, View.VISIBLE);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        setAppTitle(getString(R.string.artists_page));

        UserDetailsFragment fragment = new UserDetailsFragment(userModel);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void NewsDetailsFragment(NewsModel newsModel){

        backEnable = true;
        changeTitle(View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        setAppTitle(getString(R.string.news));

        NewsDetailsFragment fragment = new NewsDetailsFragment(newsModel);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void TrainingDetail(TrainingModel trainingModel){

        backEnable = true;
        changeTitle(View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        setAppTitle(getString(R.string.training_detail));

        TrainingDetailsFragment fragment = new TrainingDetailsFragment(trainingModel);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    @Override
    public void supportInvalidateOptionsMenu() {
        invalidateOptionsMenu();
        super.supportInvalidateOptionsMenu();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_call_drawer:
                drawerLayout.openDrawer(GravityCompat.START);
                break;

            case R.id.imv_setting:
                if (Commons.g_user != null){

                    showSettingMenu(v);

                } else {
                    showAlertDialog(getString(R.string.setting_menu));
                }
                break;

            case R.id.imv_search:
                showSearch(v);
                break;

            case R.id.imv_choose_artist:
                Commons.SEARCH_ROLE = 1;
                showToast("artist");
                selectPro(true, false, false);

                break;
            case R.id.imv_choose_style:
                Commons.SEARCH_ROLE = 2;
                showToast("style");
                selectPro(false, true, false);
                break;
            case R.id.imv_choose_model:
                Commons.SEARCH_ROLE = 3;
                showToast("model");
                selectPro(false, false, true);
                break;
        }
    }

    private void selectPro(boolean a, boolean b, boolean c){

        imv_artist.setSelected(a);
        imv_style.setSelected(b);
        imv_model.setSelected(c);
    }

    private void showSettingMenu(View v){

        android.widget.PopupMenu popupMenu = new android.widget.PopupMenu(MainActivity.this, v);
        popupMenu.inflate(R.menu.popup_setting_menu);

        Object menuHelper;
        Class[] argTypes;

        try {
            Field fMenuHelper = android.widget.PopupMenu.class.getDeclaredField("mPopup");

            fMenuHelper.setAccessible(true);
            menuHelper = fMenuHelper.get(popupMenu);
            argTypes = new Class[]{boolean.class};
            menuHelper.getClass().getDeclaredMethod("setForceShowIcon", argTypes).invoke(menuHelper, true);
        } catch (Exception e) {

            Log.w("Error====>", "error forcing menu icons to show", e);
            popupMenu.show();

            return;
        }
        popupMenu.setOnMenuItemClickListener(new android.widget.PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.item_my_account:
                        Commons.MY_PROFILE = 0;
                        gotoMyAccount();
                        return true;

                    case R.id.item_my_profile:
                        MyProfileFragment();
                        return true;
                }

                return true;
            }
        });

        popupMenu.show();
    }

    private void showSearch(View sv){

        android.widget.PopupMenu popupMenu = new android.widget.PopupMenu(MainActivity.this, sv);
        popupMenu.inflate(R.menu.popup_search_menu);

        Object menuHelper;
        Class[] argTypes;

        try {
            Field fMenuHelper = android.widget.PopupMenu.class.getDeclaredField("mPopup");

            fMenuHelper.setAccessible(true);
            menuHelper = fMenuHelper.get(popupMenu);
            argTypes = new Class[]{boolean.class};
            menuHelper.getClass().getDeclaredMethod("setForceShowIcon", argTypes).invoke(menuHelper, true);
        } catch (Exception e) {

            Log.w("Error====>", "error forcing menu icons to show", e);
            popupMenu.show();

            return;
        }
        popupMenu.setOnMenuItemClickListener(new android.widget.PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.menu_search_name:

                        Commons.SEARCH_PARAM = 1;//1:nameSearch

                       /* if (Commons.ROLE_STATE != 0){

                            Log.d("ROLE_STAT==>", String.valueOf(Commons.ROLE_STATE));
                            nameSearch();

                        } else*/ choosePro();

                        return true;

                    case R.id.menu_search_date:

                        Commons.SEARCH_PARAM = 2;    //2:dateSearch

                   /*     if (Commons.ROLE_STATE != 0){

                            //dataSearch();
                            showDatePickerDialog(v);

                        }else */choosePro();

                        return true;

                    case R.id.menu_search_location:

                        Commons.SEARCH_PARAM = 3;    //3:locationSearch

                 /*       if (Commons.ROLE_STATE != 0){
                            locationSearch();

                        } else*/ choosePro();

                        break;
                }

                return true;
            }
        });

        popupMenu.show();

    }

    private void choosePro(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_alert_select_pro);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        imv_artist = (ImageView)dialog.findViewById(R.id.imv_choose_artist);
        imv_artist.setOnClickListener(this);

        imv_style = (ImageView)dialog.findViewById(R.id.imv_choose_style);
        imv_style.setOnClickListener(this);

        imv_model = (ImageView)dialog.findViewById(R.id.imv_choose_model);
        imv_model.setOnClickListener(this);

        selectPro(true, false, false);

        Commons.SEARCH_ROLE = 1;

        Button btn_next = (Button)dialog.findViewById(R.id.btn_choose_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Commons.SEARCH_PARAM == 1) nameSearch();

                else if (Commons.SEARCH_PARAM == 2) showDatePickerDialog(v);

                else if (Commons.SEARCH_PARAM == 3) locationSearch();

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void nameSearch(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_alert_t_search);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        txv_search_title = (TextView)dialog.findViewById(R.id.txv_search_title);

        final EditText edt_content = (EditText)dialog.findViewById(R.id.edt_search_content);

        if (Commons.SEARCH_PARAM == 1){

            txv_search_title.setText(R.string.search_name);

        } else txv_search_title.setText(R.string.search_location);

        Button btn_search = (Button)dialog.findViewById(R.id.btn_t_search);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Commons.SEARCH_EDIT = edt_content.getText().toString();

                if (Commons.SEARCH_EDIT.toString().length() == 0){
                    showAlertDialog(getString(R.string.input_name));

                } else {

                    if (Commons.SEARCH_ROLE == 1 || Commons.ROLE_STATE == 1){
                        navigate(2);
                        initSearchRole(0);

                    }else if (Commons.SEARCH_ROLE == 2 || Commons.ROLE_STATE ==2){
                        navigate(3);
                        initSearchRole(0);

                    }else if (Commons.SEARCH_ROLE == 3 || Commons.ROLE_STATE == 3){
                        navigate(4);
                        initSearchRole(0);
                    }

                    dialog.dismiss();
                }

            }
        });

        dialog.show();
    }

    public void setSearchLocation(String string){

        edt_search.setText(string);
    }



    /*public void locationSearch1(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_alert_location);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        edt_search = (EditText)dialog.findViewById(R.id.edt_search_content);
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String text = edt_search.getText().toString().toLowerCase(Locale.getDefault());

                _adapter.filter(text);

            }
        });


        *//*lst_location = (ListView)dialog.findViewById(R.id.lst_location);
        _adapter = new LocationListAdapter(this);
        lst_location.setAdapter(_adapter);*//*

        Button btn_location_search = (Button)dialog.findViewById(R.id.btn_location_search);
        btn_location_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Commons.SEARCH_EDIT = edt_search.getText().toString();

                if (Commons.SEARCH_EDIT.toString().length() == 0) showAlertDialog(getString(R.string.input_add));

                else {

                    if (Commons.SEARCH_ROLE == 1 || Commons.ROLE_STATE == 1){
                        navigate(2);
                        initSearchRole(0);

                    }else if (Commons.SEARCH_ROLE == 2 || Commons.ROLE_STATE ==2){
                        navigate(3);
                        initSearchRole(0);

                    }else if (Commons.SEARCH_ROLE == 3 || Commons.ROLE_STATE == 3){
                        navigate(4);
                        initSearchRole(0);
                    }

                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }*/



    public void locationSearch(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_alert_location);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        atvfrompostalstreet = (AutoCompleteTextView) dialog.findViewById(R.id.auto_search_content);
        atvfrompostalstreet.setThreshold(1);
        atvfrompostalstreet.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        Button btn_location_search = (Button)dialog.findViewById(R.id.btn_location_search);
        btn_location_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Commons.SEARCH_EDIT = atvfrompostalstreet.getText().toString();

                if (Commons.SEARCH_EDIT.toString().length() == 0) showAlertDialog(getString(R.string.input_add));

                else {

                    if (Commons.SEARCH_ROLE == 1 || Commons.ROLE_STATE == 1){
                        navigate(2);
                        initSearchRole(0);

                    }else if (Commons.SEARCH_ROLE == 2 || Commons.ROLE_STATE ==2){
                        navigate(3);
                        initSearchRole(0);

                    }else if (Commons.SEARCH_ROLE == 3 || Commons.ROLE_STATE == 3){
                        navigate(4);
                        initSearchRole(0);
                    }

                    dialog.dismiss();
                }
            }
        });

        dialog.show();

    }


    //======================= AutoComplete========================
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            String key = "key=AIzaSyB_R9NUFK4G5lf-Zpr1wUmXDQ0WWZecYzA";

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;

            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};

            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

            // Setting the adapter
            atvfrompostalstreet.setAdapter(adapter);

        }
    }

    /**
     * A method to download json data from url
     */
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();


            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

/*


    private void dataSearch(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_alert_calendar);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        final CalendarView calendar;

        calendar = (CalendarView)dialog.findViewById(R.id.calendar_search);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {

                calendar.setBackgroundColor(getResources().getColor(R.color.bg_color));

                Toast.makeText(_context, String.valueOf(dayOfMonth), Toast.LENGTH_SHORT).show();
                Commons.YEAR = year;Commons.MONTH = String.valueOf(month); Commons.VALUE = String.valueOf(dayOfMonth);
            }
        });

        TextView btn_d_next = (TextView)dialog.findViewById(R.id.btn_d_next);
        btn_d_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Commons.YEAR== 0 && Commons.MONTH.length() == 0 && Commons.VALUE.length() == 0)
                    showAlertDialog(getString(R.string.select_day));
                else {
                    locationSearch();
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }


*/

    public void showDatePickerDialog(View v) {

        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            String date = String.valueOf(year) + "/" + String.valueOf(month) + "/" + String.valueOf(day);

            Commons.YEAR = year ; Commons.MONTH = String.valueOf(month); Commons.VALUE = String.valueOf(day);

            Log.d("=====Year==", String.valueOf(Commons.YEAR));
            Log.d("====Month==>", Commons.MONTH);
            Log.d("====Day==>", String.valueOf(Commons.VALUE));

            if (Commons.YEAR == 0 && Commons.MONTH.length() == 0 && Commons.VALUE.length() == 0)
                showAlertDialog(getString(R.string.select_day));
            else {
                locationSearch();
            }
        }
    }

    public void initParam(int a){

        Commons.SEARCH_PARAM = a;
    }

    public void initSearchRole(int b){
        Commons.SEARCH_ROLE = b;
    }


    private void gotoLogin(){

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0,0);

    }

    private void gotoMyAccount(){

        Intent intent = new Intent(this, MyAccountActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0,0);
    }

    public void changeTitle(int menu, int back, int cal, int search, int setting){

        imv_call_drawer.setVisibility(menu);
        imv_back.setVisibility(back);
        imv_calendar.setVisibility(cal);
        imv_search.setVisibility(search);
        imv_setting.setVisibility(setting);

    }

    public void currentPage(boolean a, boolean b, boolean c){

        Constants.ARTIST = a;
        Constants.STYLE = b;
        Constants.MODEL = c;
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);

        } else {

            if (!backEnable) {
                onExit();

            }else if (Commons.PROFILE_EDIT == 1){

                Commons.PROFILE_EDIT = 0;
                Commons.MY_PROFILE = 0;
                MyProfileFragment();

                /*ROLE_DETAILS //1: ARTIST, 2:STYLIST, 3:MODELS, 4:NEWS, 5:TRAINING*/

            } else if (Commons.ROLE_DETAILS == 1) {
                if (Commons.ROLE_STATE == 1){
                    Commons.ROLE_DETAILS = 0;
                    Commons.ROLE_STATE = 0;
                    navigate(2);
                }

                else {
                    Commons.ROLE_STATE = 0;
                    Commons.ROLE_DETAILS = 0;
                    navigate(1);
                    navigate(1);
                }


            }else if (Commons.ROLE_DETAILS == 2){

                if (Commons.ROLE_STATE == 2){
                    Commons.ROLE_STATE = 0;
                    Commons.ROLE_DETAILS = 0;
                    navigate(3);

                } else {
                    Commons.ROLE_STATE = 0;
                    Commons.ROLE_DETAILS = 0;
                    navigate(1);
                }


            }else if (Commons.ROLE_DETAILS == 3) {
                if (Commons.ROLE_STATE == 3){
                    Commons.ROLE_STATE = 0;
                    Commons.ROLE_DETAILS = 0;
                    navigate(4);

                } else {
                    Commons.ROLE_STATE = 0;
                    Commons.ROLE_DETAILS = 0;
                    navigate(1);
                }

            } else if (Commons.ROLE_DETAILS == 4) {

                if (Commons.ROLE_STATE == 4){

                    Commons.ROLE_STATE = 0;
                    Commons.ROLE_DETAILS = 0;
                    navigate(5);
                } else {
                    Commons.ROLE_STATE = 0;
                    Commons.ROLE_DETAILS = 0;
                    navigate(1);
                }


            } else if (Commons.ROLE_DETAILS == 5){

                if (Commons.ROLE_STATE == 5){
                    Commons.ROLE_DETAILS = 0;
                    Commons.ROLE_STATE = 0;
                    navigate(6);
                }else {
                    Commons.ROLE_STATE = 0;
                    Commons.ROLE_DETAILS = 0;
                    navigate(1);
                }

            }else {
                navigate(1);
            }
        }
    }

    @Override
    public void onDateClick(CalendarDate calendarDate) {

        AVA_YEAR = 0;
        AVA_MONTH = new ArrayList<>();
        AVA_DAY = new ArrayList<>();

        int year = calendarDate.getSolar().solarYear;
        int month = calendarDate.getSolar().solarMonth;
        int day = calendarDate.getSolar().solarDay;

        AVA_YEAR = (year);
        AVA_MONTH.add(month);
        AVA_DAY.add(day);

        mListDate.add(calendarDate);
    }

    @Override
    public void onDateCancel(CalendarDate calendarDate) {
        int count = mListDate.size();
        for (int i = 0; i < count; i++) {
            CalendarDate date = mListDate.get(i);
            if (date.getSolar().solarDay == calendarDate.getSolar().solarDay) {
                mListDate.remove(i);
                break;
            }
        }
    }

    @Override
    public void onPageChange(int year, int month) {

        Log.d("==year + month==>" , String.valueOf(year + month));

        tv_date = (TextView)findViewById(R.id.tv_date);
        tv_date.setText(year + "-" + month);
        Log.d("====Date==>", listToString(mListDate));
        mListDate.clear();

    }

    private static String listToString(List<CalendarDate> list) {
        StringBuffer stringBuffer = new StringBuffer();
        for (CalendarDate date : list) {
            stringBuffer.append(date.getSolar().solarYear + "-" + date.getSolar().solarMonth + "-" + date.getSolar().solarDay).append(" ");
        }
        return stringBuffer.toString();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        initParam(0);
        initSearchRole(0);
    }

}
