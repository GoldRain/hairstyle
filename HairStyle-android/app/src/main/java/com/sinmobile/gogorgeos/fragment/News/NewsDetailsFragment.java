package com.sinmobile.gogorgeos.fragment.News;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sinmobile.gogorgeos.R;
import com.sinmobile.gogorgeos.activity.MainActivity;
import com.sinmobile.gogorgeos.commons.Commons;
import com.sinmobile.gogorgeos.model.NewsModel;


public class NewsDetailsFragment extends Fragment {

    MainActivity _activity;
    View view;
    ImageView imv_back;

    NewsModel _newsModel = new NewsModel();

    TextView txv_newsTitle, txv_newsDate,txv_newContent;
    ImageView imv_newsImages;

    public NewsDetailsFragment(NewsModel newsModel) {
        // Required empty public constructor

        _newsModel = newsModel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_news_details, container, false);
        Commons.ROLE_DETAILS = 5;

        loadLayout();
        return view;
    }

    private void loadLayout(){

        imv_back = (ImageView)_activity.findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _activity.onBackPressed();
            }
        });
        txv_newsTitle = (TextView)view.findViewById(R.id.txv_newsTitle);
        txv_newsDate = (TextView)view.findViewById(R.id.txv_newsDate);
        txv_newContent = (TextView)view.findViewById(R.id.txv_newContent);

        imv_newsImages = (ImageView)view.findViewById(R.id.imv_newsImage);

        Glide.with(_activity).load(_newsModel.get_news_imageurl()).placeholder(R.color.news_greay).into(imv_newsImages);
        txv_newsTitle.setText(_newsModel.get_news_title());
        txv_newContent.setText(_newsModel.get_news_content());
        txv_newsDate.setText(_newsModel.get_news_date());

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}
